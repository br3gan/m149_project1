package di.postgrad.databasesystems.project1.customQuery;

import org.hibernate.Query;
import org.hibernate.Session;

public class QueryLogger {

	private Session activeSession;
	private int verboseLevel;

	public QueryLogger(Session _activeSession, int _verboseLevel) {
		activeSession = _activeSession;
		verboseLevel = _verboseLevel;
	}

	Integer logQueryForUser(String username, String userQuery) {

        activeSession.beginTransaction();

		String sql = "insert into user_queries (username, query, time) values (:username, :userQuery, now());";

		Query query = activeSession
				.createSQLQuery(sql)
				.setParameter("username", username)
				.setParameter("userQuery", userQuery);

		Integer result = 0;
		try {

			if (verboseLevel>0) {
				System.out.println("[CustomQuerys] Executing query:\n\t" + query.getQueryString());
			}

			result = query.executeUpdate();

			if (verboseLevel>1) {
				System.out.println("[CustomQuerys] \t" + result);
			}

        }
        catch(Exception e) {
        	System.err.println("Failed to excecute " + sql + ": " + e.getMessage());
        }

        activeSession.getTransaction().commit();

		return result;
	}
	
}
