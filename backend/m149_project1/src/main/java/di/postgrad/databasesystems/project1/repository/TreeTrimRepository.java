package di.postgrad.databasesystems.project1.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import di.postgrad.databasesystems.project1.domain.TreeTrim;

@Repository
public interface TreeTrimRepository extends CrudRepository<TreeTrim, Long> {

}

