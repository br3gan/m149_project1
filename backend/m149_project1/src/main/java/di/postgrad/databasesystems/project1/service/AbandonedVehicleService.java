package di.postgrad.databasesystems.project1.service;


import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import di.postgrad.databasesystems.project1.domain.AbandonedVehicle;
import di.postgrad.databasesystems.project1.repository.AbandonedVehicleRepository;

@Service
public class AbandonedVehicleService {

    @Autowired
    AbandonedVehicleRepository abandonedVehicleRepository;
    
    @Autowired
    LocationSpecificRequestsService locationSpecificRequestsService;

    public AbandonedVehicle addAbandonedVehicle(AbandonedVehicle abandonedVehicle) {
        abandonedVehicleRepository.save(abandonedVehicle);
        return abandonedVehicle;
    }

    public Iterable<AbandonedVehicle> getAbandonedVehicles(){
        return abandonedVehicleRepository.findAll();
    }

    public AbandonedVehicle getAbandonedVehicle(Long abandonedVehicleId){
      return abandonedVehicleRepository.findOne(abandonedVehicleId);
  }
    
    public Iterable<AbandonedVehicle> findLocationSpecificAbandonedVehicles(String username, String street_address, Integer zip_code){
    	
        Iterable<BigInteger> abandoned_vehicles_id = locationSpecificRequestsService.findLocationSpecificAbandonedVehicles(username, street_address, zip_code);
        List<AbandonedVehicle> abandoned_vehicle_list = new ArrayList<>();
        if (abandoned_vehicles_id != null) {
        	for (BigInteger id : abandoned_vehicles_id) {
            	abandoned_vehicle_list.add(abandonedVehicleRepository.findOne(id.longValue()));
            }
        }
        
        return (Iterable<AbandonedVehicle>) abandoned_vehicle_list;
    }
}