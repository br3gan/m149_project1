package di.postgrad.databasesystems.project1.service;


import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import di.postgrad.databasesystems.project1.domain.PotHolesReported;
import di.postgrad.databasesystems.project1.repository.PotHolesReportedRepository;

@Service
public class PotHolesReportedService {

    @Autowired
    PotHolesReportedRepository potHolesReportedRepository;

    @Autowired
    LocationSpecificRequestsService locationSpecificRequestsService;

    public PotHolesReported addPotHolesReported(PotHolesReported potHolesReported) {
        potHolesReportedRepository.save(potHolesReported);
        return potHolesReported;
    }

    public Iterable<PotHolesReported> getPotHolesReporteds(){
        return potHolesReportedRepository.findAll();
    }

    public PotHolesReported getPotHolesReported(Long potHolesReportedId){
      return potHolesReportedRepository.findOne(potHolesReportedId);
  }
    public Iterable<PotHolesReported> findLocationSpecificPotHolesReported(String username, String street_address, Integer zip_code){

        Iterable<BigInteger> pot_holes_reporteds_id = locationSpecificRequestsService.findLocationSpecificPotHolesReported(username, street_address, zip_code);
        List<PotHolesReported> pot_holes_reported_list = new ArrayList<>();
        if (pot_holes_reporteds_id != null) {
        	for (BigInteger id : pot_holes_reporteds_id) {
            	pot_holes_reported_list.add(potHolesReportedRepository.findOne(id.longValue()));
            }
        }
        
        return (Iterable<PotHolesReported>) pot_holes_reported_list;
    }
}
