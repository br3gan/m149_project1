package di.postgrad.databasesystems.project1.service;


import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import di.postgrad.databasesystems.project1.domain.GraffitiRemoval;
import di.postgrad.databasesystems.project1.repository.GraffitiRemovalRepository;

@Service
public class GraffitiRemovalService {

    @Autowired
    GraffitiRemovalRepository graffitiRemovalRepository;

    @Autowired
    LocationSpecificRequestsService locationSpecificRequestsService;

    public GraffitiRemoval addGraffitiRemoval(GraffitiRemoval graffitiRemoval) {
        graffitiRemovalRepository.save(graffitiRemoval);
        return graffitiRemoval;
    }

    public Iterable<GraffitiRemoval> getGraffitiRemovals(){
        return graffitiRemovalRepository.findAll();
    }

    public GraffitiRemoval getGraffitiRemoval(Long graffitiRemovalId){
      return graffitiRemovalRepository.findOne(graffitiRemovalId);
  }

    public Iterable<GraffitiRemoval> findLocationSpecificGraffitiRemoval(String username, String street_address, Integer zip_code){

        Iterable<BigInteger> graffiti_removals_id = locationSpecificRequestsService.findLocationSpecificGraffitiRemoval(username, street_address, zip_code);
        List<GraffitiRemoval> graffiti_removal_list = new ArrayList<>();
        if (graffiti_removals_id != null) {
        	for (BigInteger id : graffiti_removals_id) {
            	graffiti_removal_list.add(graffitiRemovalRepository.findOne(id.longValue()));
            }
        }
        
        return (Iterable<GraffitiRemoval>) graffiti_removal_list;
    }
}
