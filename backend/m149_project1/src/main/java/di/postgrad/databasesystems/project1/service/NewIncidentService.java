package di.postgrad.databasesystems.project1.service;

import java.util.Date;

import org.hibernate.Session;
import org.springframework.stereotype.Service;

import di.postgrad.databasesystems.project1.customQuery.CustomQuerySessionGenerator;
import di.postgrad.databasesystems.project1.customQuery.NewIncidentAdder;

@Service
public class  NewIncidentService{

	private final Session session = CustomQuerySessionGenerator.getSessionFactory().openSession();
	private final NewIncidentAdder nia = new NewIncidentAdder(session, 2);

	@Override
	protected void finalize() {
		CustomQuerySessionGenerator.shutdown();
		session.close();
	}

    public int addAbvandonedVehicleComplaint(
            String username,
    		Date creation_date,
    		String status,
    		Date completion_date,
    		String service_request_number,
    		String type_of_service_request,
    		String license_plate,
    		String vehicle_make_model,
    		String vehicle_color,
    		String current_activity,
    		String most_recent_action,
    		Float how_many_days_has_the_vehicle_been_reported_as_parked,
    		String street_address,
    		Integer zip_code,
    		Float x_coordinate,
    		Float y_coordinate,
    		Short ward,
    		Short police_district,
    		Short community_area,
    		Short ssa,
    		Float latitude,
    		Float longitude,
    		String location)
    {
    	int result = nia.addAbandonedVehicleComplaint(username, creation_date, status, completion_date, service_request_number, type_of_service_request, license_plate, vehicle_make_model, vehicle_color, current_activity, most_recent_action, how_many_days_has_the_vehicle_been_reported_as_parked, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, ssa, latitude, longitude, location); return result;
    }

    public int addAlleyLightsOut(
            String username,
    		Date creation_date,
    		String status,
    		Date completion_date,
    		String service_request_number,
    		String type_of_service_request,
    		String street_address,
    		Integer zip_code,
    		Float x_coordinate,
    		Float y_coordinate,
    		Short ward,
    		Short police_district,
    		Short community_area,
    		Float latitude,
    		Float longitude,
    		String location)
    { int result = nia.addAlleyLightsOut(username, creation_date, status, completion_date, service_request_number, type_of_service_request, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, latitude, longitude, location);
    	return result;
    }

	public int addGarbageCarts(
            String username,
			Date creation_date,
			String status,
			Date completion_date,
			String service_request_number,
			String type_of_service_request,
			String current_activity,
			String most_recent_action,
			String street_address,
			Integer zip_code,
			Float x_coordinate,
			Float y_coordinate,
			Short ward,
			Short police_district,
			Short community_area,
			Short ssa,
			Float latitude,
			Float longitude,
			String location,
			Float number_of_black_carts_delivered)
	{
    	int result = nia.addGarbageCarts(username, creation_date, status, completion_date, service_request_number, type_of_service_request, current_activity, most_recent_action, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, ssa, latitude, longitude, location, number_of_black_carts_delivered);
    	return result;
    }

	public int addGraffitiRemoval(
            String username,
			Date creation_date,
			String status,
			Date completion_date,
			String service_request_number,
			String type_of_service_request,
			String street_address,
			Integer zip_code,
			Float x_coordinate,
			Float y_coordinate,
			Short ward,
			Short police_district,
			Short community_area,
			Short ssa,
			Float latitude,
			Float longitude,
			String location,
			String what_type_of_surface_is_the_graffiti_on,
			String where_is_the_graffiti_located)
	{
		int result = nia.addGraffitiRemoval(username,  creation_date, status, completion_date, service_request_number, type_of_service_request, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, ssa, latitude, longitude, location, what_type_of_surface_is_the_graffiti_on, where_is_the_graffiti_located);
		return result;
	}

	public int addPotholesReported(
            String username,
			Date creation_date,
			String status,
			Date completion_date,
			String service_request_number,
			String type_of_service_request,
			String current_activity,
			String most_recent_action,
			String street_address,
			Integer zip_code,
			Float x_coordinate,
			Float y_coordinate,
			Short ward,
			Short police_district,
			Short community_area,
			Short ssa,
			Float latitude,
			Float longitude,
			String location,
			Float number_of_potholes_filled_on_block)
	{
		int result = nia.addPotholesReported(username, creation_date, status, completion_date, service_request_number, type_of_service_request, current_activity, most_recent_action, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, ssa, latitude, longitude, location, number_of_potholes_filled_on_block);
		return result;
	}

	public int addRodentBaiting(
            String username,
			Date creation_date,
			String status,
			Date completion_date,
			String service_request_number,
			String type_of_service_request,
			String current_activity,
			String most_recent_action,
			String street_address,
			Integer zip_code,
			Float x_coordinate,
			Float y_coordinate,
			Short ward,
			Short police_district,
			Short community_area,
			Float latitude,
			Float longitude,
			String location,
			Float number_of_premises_baited,
			Float number_of_premises_with_garbage,
			Float number_of_premises_with_rats)
	{
		int result = nia.addRodentBaiting(username, creation_date, status, completion_date, service_request_number, type_of_service_request, current_activity, most_recent_action, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, latitude, longitude, location, number_of_premises_baited, number_of_premises_with_garbage, number_of_premises_with_rats);
		return result;
	}

	public int addSanitationCodeComplaints(
            String username,
			Date creation_date,
			String status,
			Date completion_date,
			String service_request_number,
			String type_of_service_request,
			String street_address,
			Integer zip_code,
			Float x_coordinate,
			Float y_coordinate,
			Short ward,
			Short police_district,
			Short community_area,
			Float latitude,
			Float longitude,
			String location,
			String what_is_the_nature_of_this_code_violation)
	{
		int result = nia.addSanitationCodeComplaints(username, creation_date, status, completion_date, service_request_number, type_of_service_request, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, latitude, longitude, location, what_is_the_nature_of_this_code_violation);
		return result;
	}

	public int addStreetLightsAllOut(
            String username,
			Date creation_date,
			String status,
			Date completion_date,
			String service_request_number,
			String type_of_service_request,
			String street_address,
			Integer zip_code,
			Float x_coordinate,
			Float y_coordinate,
			Short ward,
			Short police_district,
			Short community_area,
			Float latitude,
			Float longitude,
			String location)
	{
		int result = nia.addStreetLightsAllOut(username, creation_date, status, completion_date, service_request_number, type_of_service_request, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, latitude, longitude, location);
		return result;
	}

	public int addStreetLightsOneOut(
            String username,
			Date creation_date,
			String status,
			Date completion_date,
			String service_request_number,
			String type_of_service_request,
			String street_address,
			Integer zip_code,
			Float x_coordinate,
			Float y_coordinate,
			Short ward,
			Short police_district,
			Short community_area,
			Float latitude,
			Float longitude,
			String location)
	{
		int result = nia.addStreetLightsOneOut(username, creation_date, status, completion_date, service_request_number, type_of_service_request, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, latitude, longitude, location);
		return result;
	}

	public int addTreeDebris(
            String username,
			Date creation_date,
			String status,
			Date completion_date,
			String service_request_number,
			String type_of_service_request,
			String current_activity,
			String most_recent_action,
			String street_address,
			Integer zip_code,
			Float x_coordinate,
			Float y_coordinate,
			Short ward,
			Short police_district,
			Short community_area,
			Float latitude,
			Float longitude,
			String location,
			String if_yes_where_is_the_debris_located)
	{
		int result = nia.addTreeDebris(username, creation_date, status, completion_date, service_request_number, type_of_service_request, current_activity, most_recent_action, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, latitude, longitude, location, if_yes_where_is_the_debris_located);
		return result;
	}

	public int addTreeTrims(
            String username,
			Date creation_date,
			String status,
			Date completion_date,
			String service_request_number,
			String type_of_service_request,
			String street_address,
			Integer zip_code,
			Float x_coordinate,
			Float y_coordinate,
			Short ward,
			Short police_district,
			Short community_area,
			Float latitude,
			Float longitude,
			String location,
			String location_of_trees)
	{
		int result = nia.addTreeTrims(username, creation_date, status, completion_date, service_request_number, type_of_service_request, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, latitude, longitude, location, location_of_trees);
		return result;
	}
}





















