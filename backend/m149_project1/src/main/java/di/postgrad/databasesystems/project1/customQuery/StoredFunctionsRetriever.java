package di.postgrad.databasesystems.project1.customQuery;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

public class StoredFunctionsRetriever{

	private Session activeSession;
	private int verboseLevel;
	private QueryLogger queryLogger;

	public StoredFunctionsRetriever(Session _activeSession, int _verboseLevel) {
		activeSession = _activeSession;
		verboseLevel = _verboseLevel;
		queryLogger = new QueryLogger(_activeSession, _verboseLevel);
	}

	/**
	 * @param startingDate
	 * @param endingDate
	 * @return Iterable of Object[]. Object[0] = type_of_service_request, Object[1] = counter
	 */
	@SuppressWarnings("unchecked")
	public Iterable<Object[]> getFunction01(Date startingDate, Date endingDate, String username) {

        String sqlLog = "SELECT * FROM function_01('" + startingDate + "', '" + endingDate + "');";
        queryLogger.logQueryForUser(username, sqlLog);

        activeSession.beginTransaction();

        String sql = "SELECT * FROM function_01(:startingDate, :endingDate);";
        Iterable<Object[]> result = null;

        try {

        	System.out.println(username);

        	Query query = activeSession
        			.createSQLQuery(sql);

        	sql = query.getQueryString();
        	result = (List<Object[]>) query
        									.setParameter("startingDate", startingDate)
        									.setParameter("endingDate", endingDate)
                                            .list();

        	if (verboseLevel>0) {
				System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
			}
        	if (verboseLevel>1) {
        		System.out.println("[CustomQuery] Output:");
        		for(Object[] r : result){
       				System.out.print("\t");
        			for (int i = 0; i < r.length - 1; i++) {
        				System.out.print(r[i] + ", ");
        			}
       				System.out.println("\t" + r[r.length - 1] );
        		}
        	}
        }
        catch(Exception e) {
        	System.err.println("Failed to excecute " + sql + ": " + e.getMessage());
        }

        activeSession.getTransaction().commit();
        return result;
	}

	/**
	 * @param typeOfServiceRequest
	 * @param startingDate
	 * @param endingDate
	 * @return Iterable of Object[]. Object[0] = creationDate, Object[1] = counter
	 */
	@SuppressWarnings("unchecked")
	public Iterable<Object[]> getFunction02(String typeOfServiceRequest, Date startingDate, Date endingDate, String username) {

        String sqlLog = "SELECT * FROM function_02('" + typeOfServiceRequest + "' ,'" + startingDate + "', '" + endingDate + "');";
        queryLogger.logQueryForUser(username, sqlLog);

        activeSession.beginTransaction();

        String sql = "SELECT * FROM function_02(:typeOfServiceRequest ,:startingDate, :endingDate);";

        if (verboseLevel>0) {
        	System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
        }
        Iterable<Object[]> result = null;

        try {

        	Query query = activeSession.createSQLQuery(sql);

        	sql = query.getQueryString();
            result = (List<Object[]>) query
                .setParameter("typeOfServiceRequest", typeOfServiceRequest)
                .setParameter("startingDate", startingDate)
                .setParameter("endingDate", endingDate)
                .list();

        	if (verboseLevel>1) {
        		System.out.println("[CustomQuery] Output:");
        		for(Object[] r : result){
       				System.out.print("\t");
        			for (int i = 0; i < r.length - 1; i++) {
        				System.out.print(r[i] + ", ");
        			}
       				System.out.println("\t" + r[r.length - 1] );
        		}
        	}
        }
        catch(Exception e) {
        	System.err.println("Failed to excecute " + sql + ": " + e.getMessage());
        }

        activeSession.getTransaction().commit();
        return result;
	}

	/**
	 * @param date
	 * @return Iterable of Object[]. Object[0] = type_of_service, Object[1] = zip_code, Object[2] = counter
	 */
	@SuppressWarnings("unchecked")
	public Iterable<Object[]> getFunction03(Date date, String username) {
        String sqlLog = "SELECT * FROM function_03('" + date + "');";
        queryLogger.logQueryForUser(username, sqlLog);

        activeSession.beginTransaction();

        String sql = "SELECT * FROM function_03(:date);";

        if (verboseLevel>0) {
        	System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
        }

        Iterable<Object[]> result = null;

        try {

        	Query query = activeSession.createSQLQuery(sql);

        	sql = query.getQueryString();
            result = (List<Object[]>) query
                .setParameter("date", date)
                .list();
        	if (verboseLevel>1) {
        		System.out.println("[CustomQuery] Output:");
        		for(Object[] r : result){
       				System.out.print("\t");
        			for (int i = 0; i < r.length - 1; i++) {
        				System.out.print(r[i] + ", ");
        			}
       				System.out.println("\t" + r[r.length - 1] );
        		}
        	}
        }
        catch(Exception e) {
        	System.err.println("Failed to excecute " + sql + ": " + e.getMessage());
        }

        activeSession.getTransaction().commit();
        return result;
	}

	/**
	 * @param startingDate
	 * @param endingDate
	 * @return Iterable of Object[]. Object[0] = average, Object[1] = type_of_service_request
	 */
	@SuppressWarnings("unchecked")
	public Iterable<Object[]> getFunction04(Date startingDate, Date endingDate, String username) {

        String sqlLog = "SELECT * FROM function_04('" + startingDate + "', '" + endingDate + "');";
        queryLogger.logQueryForUser(username, sqlLog);

        activeSession.beginTransaction();

        String sql = "SELECT * FROM function_04(:startingDate, :endingDate);";

        if (verboseLevel>0) {
        	System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
        }

        Iterable<Object[]> result = null;

        try {
        	Query query = activeSession.createSQLQuery(sql);

        	sql = query.getQueryString();
            result = (List<Object[]>) query
                .setParameter("startingDate", startingDate)
                .setParameter("endingDate", endingDate)
                .list();
        	if (verboseLevel>1) {
        		System.out.println("[CustomQuery] Output:");
        		for(Object[] r : result){
       				System.out.print("\t");
        			for (int i = 0; i < r.length - 1; i++) {
        				System.out.print(r[i] + ", ");
        			}
       				System.out.println("\t" + r[r.length - 1] );
        		}
        	}
        }
        catch(Exception e) {
        	System.err.println("Failed to excecute " + sql + ": " + e.getMessage());
        }

        activeSession.getTransaction().commit();
        return result;
	}

	/**
	 * @param startingDate
	 * @param endingDate
	 * @return Iterable of Object[]. Object[0] = type_of_service_request, Object[1] = count
	 */
	@SuppressWarnings("unchecked")
	public Iterable<Object[]> getFunction05(Float min_lon, Float max_lon, Float min_lat, Float max_lat, Date creationDate, String username) {

        String sqlLog = "SELECT * FROM function_05(" + min_lon + ", " + max_lon + ", " + min_lat + ", " + max_lat + ", '" + creationDate + "');";
        queryLogger.logQueryForUser(username, sqlLog);

        activeSession.beginTransaction();

        String sql = "SELECT * FROM function_05(:min_lon, :max_lon, :min_lat, :max_lat, :creationDate);";

        if (verboseLevel>0) {
        	System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
        }

        Iterable<Object[]> result = null;

        try {
        	Query query = activeSession.createSQLQuery(sql);

        	sql = query.getQueryString();
            result = (List<Object[]>) query
                .setParameter("min_lon", min_lon)
                .setParameter("max_lon", max_lon)
                .setParameter("min_lat", min_lat)
                .setParameter("max_lat", max_lat)
                .setParameter("creationDate", creationDate)
                .list();
        	if (verboseLevel>1) {
        		System.out.println("[CustomQuery] Output:");
        		for(Object[] r : result){
       				System.out.print("\t");
        			for (int i = 0; i < r.length - 1; i++) {
        				System.out.print(r[i] + ", ");
        			}
       				System.out.println("\t" + r[r.length - 1] );
        		}
        	}
        }
        catch(Exception e) {
        	System.err.println("Failed to excecute " + sql + ": " + e.getMessage());
        }

        activeSession.getTransaction().commit();
        return result;
	}

	/**
	 * @param startingDate
	 * @param endingDate
	 * @return Iterable of Object[]. Object[0] = ssa, Object[2] = requests_per_day
	 */
	@SuppressWarnings("unchecked")
	public Iterable<Object[]> getFunction06(Date startingDate, Date endingDate, String username) {
        String sqlLog = "SELECT * FROM function_06('" + startingDate + "', '" + endingDate + "');";
        queryLogger.logQueryForUser(username, sqlLog);

        activeSession.beginTransaction();

        String sql = "SELECT * FROM function_06(:startingDate, :endingDate);";


        if (verboseLevel>0) {
        	System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
        }

        Iterable<Object[]> result = null;

        try {
        	Query query = activeSession.createSQLQuery(sql);

        	sql = query.getQueryString();
            result = (List<Object[]>) query
                .setParameter("startingDate", startingDate)
                .setParameter("endingDate", endingDate)
                .list();
        	if (verboseLevel>1) {
        		System.out.println("[CustomQuery] Output:");
        		for(Object[] r : result){
       				System.out.print("\t");
        			for (int i = 0; i < r.length - 1; i++) {
        				System.out.print(r[i] + ", ");
        			}
       				System.out.println("\t" + r[r.length - 1] );
        		}
        	}
        }
        catch(Exception e) {
        	System.err.println("Failed to excecute " + sql + ": " + e.getMessage());
        }

        activeSession.getTransaction().commit();
        return result;
	}


	/**
	 * @return Iterable of Object[]. Object[0] = license_plate, Object[1] = count
	 */
	@SuppressWarnings("unchecked")
	public Iterable<Object[]> getFunction07(String username) {

        String sqlLog = "SELECT * FROM function_07();";
        queryLogger.logQueryForUser(username, sqlLog);

        activeSession.beginTransaction();

        String sql = "SELECT * FROM function_07();";

        if (verboseLevel>0) {
        	System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
        }

        Iterable<Object[]> result = null;

        try {
        	Query query = activeSession.createSQLQuery(sql);;

        	sql = query.getQueryString();
        	result = (List<Object[]>) query
                .list();

        	if (verboseLevel>1) {
        		System.out.println("[CustomQuery] Output:");
        		for(Object[] r : result){
       				System.out.print("\t");
        			for (int i = 0; i < r.length - 1; i++) {
        				System.out.print(r[i] + ", ");
        			}
       				System.out.println("\t" + r[r.length - 1] );
        		}
        	}
        }
        catch(Exception e) {
        	System.err.println("Failed to excecute " + sql + ": " + e.getMessage());
        }

        activeSession.getTransaction().commit();
        return result;
	}


	/**
	 * @return Iterable of Object[]. Object[0] = colour, Object[1] = count
	 */
	@SuppressWarnings("unchecked")
	public Iterable<Object[]> getFunction08(String username) {

        String sqlLog = "SELECT * FROM function_08();";
        queryLogger.logQueryForUser(username, sqlLog);

        activeSession.beginTransaction();

        String sql = "SELECT * FROM function_08();";

        if (verboseLevel>0) {
        	System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
        }

        Iterable<Object[]> result = null;

        try {
        	Query query = activeSession.createSQLQuery(sql);;

        	sql = query.getQueryString();
        	result = (List<Object[]>) query
                .list();

        	if (verboseLevel>1) {
        		System.out.println("[CustomQuery] Output:");
        		for(Object[] r : result){
       				System.out.print("\t");
        			for (int i = 0; i < r.length - 1; i++) {
        				System.out.print(r[i] + ", ");
        			}
       				System.out.println("\t" + r[r.length - 1] );
        		}
        	}
        }
        catch(Exception e) {
        	System.err.println("Failed to excecute " + sql + ": " + e.getMessage());
        }

        activeSession.getTransaction().commit();
        return result;
	}


	/**
	 * @param maxNumberOfPremisesBaited
	 * @return Iterable of Object[]. Object[0] = creation_date, Object[1] = status, Object[2] = completion_date, Object[3] = service_request_number, Object[4] = type_of_service_request, Object[5] = number_of_premises_baited, Object[6] = number_of_premises_with_garbage, Object[7] = number_of_premises_with_rats, Object[8] = current_activity, Object[9] = most_recent_action, Object[10] = street_address, Object[11] = zip_code, Object[12] = x_coordinate, Object[13] = y_coordinate, Object[14] = ward, Object[15] = police_district, Object[16] = community_area, Object1[17] = latitude, Object[18] = longitude, Object[19] = location
	 */
	@SuppressWarnings("unchecked")
	public Iterable<Object[]> getFunction09(Float maxNumberOfPremisesBaited, String username) {

        String sqlLog = "SELECT * FROM function_09(" + maxNumberOfPremisesBaited + ");";
        queryLogger.logQueryForUser(username, sqlLog);

        activeSession.beginTransaction();

        String sql = "SELECT * FROM function_09(:maxNumberOfPremisesBaited);";

        if (verboseLevel>0) {
        	System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
        }

        Iterable<Object[]> result = null;

        try {
        	Query query = activeSession.createSQLQuery(sql);

        	sql = query.getQueryString();
        	result = (List<Object[]>) query
                .setParameter("maxNumberOfPremisesBaited", maxNumberOfPremisesBaited)
                .list();
        	if (verboseLevel>1) {
        		System.out.println("[CustomQuery] Output:");
        		for(Object[] r : result){
       				System.out.print("\t");
        			for (int i = 0; i < r.length - 1; i++) {
        				System.out.print(r[i] + ", ");
        			}
       				System.out.println("\t" + r[r.length - 1] );
        		}
        	}
        }
        catch(Exception e) {
        	System.err.println("Failed to excecute " + sql + ": " + e.getMessage());
        }

        activeSession.getTransaction().commit();
        return result;
	}


	/**
	 * @param maxNumberOfPremisesWithGarbage
	 * @return Iterable of Object[]. Object[0] = creation_date, Object[1] = status, Object[2] = completion_date, Object[3] = service_request_number, Object[4] = type_of_service_request, Object[5] = number_of_premises_baited, Object[6] = number_of_premises_with_garbage, Object[7] = number_of_premises_with_rats, Object[8] = current_activity, Object[9] = most_recent_action, Object[10] = street_address, Object[11] = zip_code, Object[12] = x_coordinate, Object[13] = y_coordinate, Object[14] = ward, Object[15] = police_district, Object[16] = community_area, Object1[17] = latitude, Object[18] = longitude, Object[19] = location
	 */
	@SuppressWarnings("unchecked")
	public Iterable<Object[]> getFunction10(Float maxNumberOfPremisesWithGarbage, String username) {

        String sqlLog = "SELECT * FROM function_10(" + maxNumberOfPremisesWithGarbage + ");";
        queryLogger.logQueryForUser(username, sqlLog);

        activeSession.beginTransaction();

        String sql = "SELECT * FROM function_10(:maxNumberOfPremisesWithGarbage);";

        if (verboseLevel>0) {
        	System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
        }

        Iterable<Object[]> result = null;

        try {
        	Query query = activeSession.createSQLQuery(sql);

        	sql = query.getQueryString();
            result = (List<Object[]>) query
                .setParameter("maxNumberOfPremisesWithGarbage", maxNumberOfPremisesWithGarbage)
                .list();
        	if (verboseLevel>1) {
        		System.out.println("[CustomQuery] Output:");
        		for(Object[] r : result){
       				System.out.print("\t");
        			for (int i = 0; i < r.length - 1; i++) {
        				System.out.print(r[i] + ", ");
        			}
       				System.out.println("\t" + r[r.length - 1] );
        		}
        	}
        }
        catch(Exception e) {
        	System.err.println("Failed to excecute " + sql + ": " + e.getMessage());
        }

        activeSession.getTransaction().commit();
        return result;
	}


	/**
	 * @param maxNumberOfPremisesWithRats
	 * @return Iterable of Object[]. Object[0] = creation_date, Object[1] = status, Object[2] = completion_date, Object[3] = service_request_number, Object[4] = type_of_service_request, Object[5] = number_of_premises_baited, Object[6] = number_of_premises_with_garbage, Object[7] = number_of_premises_with_rats, Object[8] = current_activity, Object[9] = most_recent_action, Object[10] = street_address, Object[11] = zip_code, Object[12] = x_coordinate, Object[13] = y_coordinate, Object[14] = ward, Object[15] = police_district, Object[16] = community_area, Object1[17] = latitude, Object[18] = longitude, Object[19] = location
	 */
	@SuppressWarnings("unchecked")
	public Iterable<Object[]> getFunction11(Float maxNumberOfPremisesWithRats, String username) {

        String sqlLog = "SELECT * FROM function_11(" + maxNumberOfPremisesWithRats + ");";
        queryLogger.logQueryForUser(username, sqlLog);

        activeSession.beginTransaction();

        String sql = "SELECT * FROM function_11(:maxNumberOfPremisesWithRats);";

        if (verboseLevel>0) {
        	System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
        }

        Iterable<Object[]> result = null;

        try {
        	Query query = activeSession.createSQLQuery(sql);

        	sql = query.getQueryString();
            result = (List<Object[]>) query
                .setParameter("maxNumberOfPremisesWithRats", maxNumberOfPremisesWithRats)
                .list();
        	if (verboseLevel>1) {
        		System.out.println("[CustomQuery] Output:");
        		for(Object[] r : result){
       				System.out.print("\t");
        			for (int i = 0; i < r.length - 1; i++) {
        				System.out.print(r[i] + ", ");
        			}
       				System.out.println("\t" + r[r.length - 1] );
        		}
        	}
        }
        catch(Exception e) {
        	System.err.println("Failed to excecute " + sql + ": " + e.getMessage());
        }

        activeSession.getTransaction().commit();
        return result;
	}


	/**
	 * @param numberOfPotholesFilledOnBlock
	 * @param numberOfPremisesBaited
	 * @param completionDate
	 * @return Iterable of Object[]. Object[0] = police_district
	 */
	@SuppressWarnings("unchecked")
	public Iterable<Object> getFunction12(Float numberOfPotholesFilledOnBlock, Float numberOfPremisesBaited, Date completionDate, String username) {

        String sqlLog = "SELECT * FROM function_12(" + numberOfPotholesFilledOnBlock + ", " + numberOfPremisesBaited + ", '" + completionDate + "');";
        queryLogger.logQueryForUser(username, sqlLog);

        activeSession.beginTransaction();

        String sql = "SELECT * FROM function_12(:numberOfPotholesFilledOnBlock, :numberOfPremisesBaited, :completionDate);";

        if (verboseLevel>0) {
        	System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
        }

        Iterable<Object> result = null;

        try {
        	Query query = activeSession.createSQLQuery(sql);

        	sql = query.getQueryString();
            result = (List<Object>) query
                .setParameter("numberOfPotholesFilledOnBlock", numberOfPotholesFilledOnBlock)
                .setParameter("numberOfPremisesBaited", numberOfPremisesBaited)
                .setParameter("completionDate", completionDate)
                .list();
        	if (verboseLevel>1) {
        		for(Object r : result){
       				System.out.println("\t" + r );
        		}
        	}
        }
        catch(Exception e) {
        	System.err.println("Failed to excecute " + sql + ": " + e.getMessage());
        }

        activeSession.getTransaction().commit();
        return result;
	}
}
