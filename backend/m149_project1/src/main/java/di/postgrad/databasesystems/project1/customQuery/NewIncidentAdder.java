package di.postgrad.databasesystems.project1.customQuery;

import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.annotations.Where;

public class NewIncidentAdder{

    private Session activeSession;
    private int verboseLevel;
    private QueryLogger queryLogger;

    public NewIncidentAdder(Session _activeSession, int _verboseLevel) {
        activeSession = _activeSession;
        verboseLevel = _verboseLevel;
        queryLogger = new QueryLogger(_activeSession, _verboseLevel);
    }

    @SuppressWarnings("unchecked")
    public int addAbandonedVehicleComplaint(
            String username,
            Date creation_date,
            String status,
            Date completion_date,
            String service_request_number,
            String type_of_service_request,
            String license_plate,
            String vehicle_make_model,
            String vehicle_color,
            String current_activity,
            String most_recent_action,
            Float how_many_days_has_the_vehicle_been_reported_as_parked,
            String street_address,
            Integer zip_code,
            Float x_coordinate,
            Float y_coordinate,
            Short ward,
            Short police_district,
            Short community_area,
            Short ssa,
            Float latitude,
            Float longitude,
            String location) {

                String sqlLog = "insert into abandoned_vehicles_tmp (creation_date, status, completion_date, service_request_number, type_of_service_request, license_plate, vehicle_make_model, vehicle_color, current_activity, most_recent_action, how_many_days_has_the_vehicle_been_reported_as_parked, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, ssa, latitude, longitude, location) VALUES ('" + creation_date + "', '" + status + "', '" + completion_date + "', '" + service_request_number + "', '" + type_of_service_request + "', '" + license_plate + "', '" + vehicle_make_model + "', '" + vehicle_color + "', '" + current_activity + "', '" + most_recent_action + "', " + how_many_days_has_the_vehicle_been_reported_as_parked + ", '" + street_address + "', " + zip_code + ", " + x_coordinate + ", " + y_coordinate + ", '" + ward + "', " + police_district + ", " + community_area + ", " + ssa + ", " + latitude + ", " + longitude + ", '" + location + "');";
                queryLogger.logQueryForUser(username, sqlLog);

                activeSession.beginTransaction();

                String sql = "insert into abandoned_vehicles_tmp (creation_date, status, completion_date, service_request_number, type_of_service_request, license_plate, vehicle_make_model, vehicle_color, current_activity, most_recent_action, how_many_days_has_the_vehicle_been_reported_as_parked, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, ssa, latitude, longitude, location) VALUES (:creation_date, :status, :completion_date, :service_request_number, :type_of_service_request, :license_plate, :vehicle_make_model, :vehicle_color, :current_activity, :most_recent_action, :how_many_days_has_the_vehicle_been_reported_as_parked, :street_address, :zip_code, :x_coordinate, :y_coordinate, :ward, :police_district, :community_area, :ssa, :latitude, :longitude, :location);";

                if (verboseLevel>0) {

                    System.out.println("[CustomQuerys] Executing query:\n\t" + sql);

                }



                int result = 0;

                try {

                    result = activeSession.createSQLQuery(sql)
                        .setParameter("location", location)
                        .setParameter("longitude", longitude)
                        .setParameter("latitude", latitude)
                        .setParameter("ssa", ssa)
                        .setParameter("community_area", community_area)
                        .setParameter("police_district", police_district)
                        .setParameter("ward", ward)
                        .setParameter("y_coordinate", y_coordinate)
                        .setParameter("x_coordinate", x_coordinate)
                        .setParameter("zip_code", zip_code)
                        .setParameter("street_address", street_address)
                        .setParameter("how_many_days_has_the_vehicle_been_reported_as_parked", how_many_days_has_the_vehicle_been_reported_as_parked)
                        .setParameter("most_recent_action", most_recent_action)
                        .setParameter("current_activity", current_activity)
                        .setParameter("vehicle_color", vehicle_color)
                        .setParameter("vehicle_make_model", vehicle_make_model)
                        .setParameter("license_plate", license_plate)
                        .setParameter("type_of_service_request", type_of_service_request)
                        .setParameter("service_request_number", service_request_number)
                        .setParameter("completion_date", completion_date)
                        .setParameter("status", status)
                        .setParameter("creation_date", creation_date)
                        .executeUpdate();

                    if (verboseLevel>1) {
                        System.out.println("[CustomQuery] Output:");
                        System.out.println("\t" + result );
                    }
                }
                catch(Exception e) {
                    System.err.println("Failed to excecute " + sql + ": " + e.getMessage());
                }

                activeSession.getTransaction().commit();
                return result;
            }

    @SuppressWarnings("unchecked")
    public int addAlleyLightsOut(
            String username,
            Date creation_date,
            String status,
            Date completion_date,
            String service_request_number,
            String type_of_service_request,
            String street_address,
            Integer zip_code,
            Float x_coordinate,
            Float y_coordinate,
            Short ward,
            Short police_district,
            Short community_area,
            Float latitude,
            Float longitude,
            String location) {
        String sqlLog = "insert into alley_lights_out_tmp (creation_date, status, completion_date, service_request_number, type_of_service_request, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, latitude, longitude, location) VALUES ('" + creation_date + "', '" + status + "', '" + completion_date + "', '" + service_request_number + "', '" + type_of_service_request + "', '" + street_address + "', " + zip_code + ", " + x_coordinate + ", " + y_coordinate + ", " + ward + ", " + police_district + ", " + community_area + ", " + latitude + ", " + longitude + ", '" + location + "');";
        queryLogger.logQueryForUser(username, sqlLog);

        activeSession.beginTransaction();

        String sql = "insert into alley_lights_out_tmp (creation_date, status, completion_date, service_request_number, type_of_service_request, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, latitude, longitude, location) VALUES (:creation_date, :status, :completion_date, :service_request_number, :type_of_service_request, :street_address, :zip_code, :x_coordinate, :y_coordinate, :ward, :police_district, :community_area, :latitude, :longitude, :location);";

        if (verboseLevel>0) {
            System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
        }

        int result = 0;
        try {
            result = activeSession.createSQLQuery(sql)
                .setParameter("location", location)
                .setParameter("longitude", longitude)
                .setParameter("latitude", latitude)
                .setParameter("community_area", community_area)
                .setParameter("police_district", police_district)
                .setParameter("ward", ward)
                .setParameter("y_coordinate", y_coordinate)
                .setParameter("x_coordinate", x_coordinate)
                .setParameter("zip_code", zip_code)
                .setParameter("street_address", street_address)
                .setParameter("type_of_service_request", type_of_service_request)
                .setParameter("service_request_number", service_request_number)
                .setParameter("completion_date", completion_date)
                .setParameter("status", status)
                .setParameter("creation_date", creation_date)
                .executeUpdate();

            if (verboseLevel>1) {
                System.out.println("[CustomQuery] Output:");
                System.out.println("\t" + result );
            }
        }
        catch(Exception e) {
            System.err.println("Failed to excecute " + sql + ": " + e.getMessage());
        }

        activeSession.getTransaction().commit();
        return result;
            }

    @SuppressWarnings("unchecked")
    public int addGarbageCarts(
            String username,
            Date creation_date,
            String status,
            Date completion_date,
            String service_request_number,
            String type_of_service_request,
            String current_activity,
            String most_recent_action,
            String street_address,
            Integer zip_code,
            Float x_coordinate,
            Float y_coordinate,
            Short ward,
            Short police_district,
            Short community_area,
            Short ssa,
            Float latitude,
            Float longitude,
            String location,
            Float number_of_black_carts_delivered) {

        String sqlLog = "insert into garbage_carts_tmp (creation_date, status, completion_date, service_request_number, type_of_service_request, current_activity, most_recent_action, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, ssa, latitude, longitude, location, number_of_black_carts_delivered) VALUES ('" + creation_date + "', '" + status + "', '" + completion_date + "', '" + service_request_number + "', '" + type_of_service_request + "', '" + current_activity + "', '" + most_recent_action + "', '" + street_address + "', " + zip_code + ", " + x_coordinate + ", " + y_coordinate + ", " + ward + ", " + police_district + ", " + community_area + ", " + ssa + ", " + latitude + ", " + longitude + ", '" + location + "', " + number_of_black_carts_delivered + ");";
        queryLogger.logQueryForUser(username, sqlLog);

        activeSession.beginTransaction();

        String sql = "insert into garbage_carts_tmp (creation_date, status, completion_date, service_request_number, type_of_service_request, current_activity, most_recent_action, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, ssa, latitude, longitude, location, number_of_black_carts_delivered) VALUES (:creation_date, :status, :completion_date, :service_request_number, :type_of_service_request, :current_activity, :most_recent_action, :street_address, :zip_code, :x_coordinate, :y_coordinate, :ward, :police_district, :community_area, :ssa, :latitude, :longitude, :location, :number_of_black_carts_delivered);";

        if (verboseLevel>0) {
            System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
        }

        int result = 0;
        try {
            result = activeSession.createSQLQuery(sql)
                .setParameter("location", location)
                .setParameter("longitude", longitude)
                .setParameter("latitude", latitude)
                .setParameter("ssa", ssa)
                .setParameter("community_area", community_area)
                .setParameter("police_district", police_district)
                .setParameter("ward", ward)
                .setParameter("y_coordinate", y_coordinate)
                .setParameter("x_coordinate", x_coordinate)
                .setParameter("zip_code", zip_code)
                .setParameter("street_address", street_address)
                .setParameter("most_recent_action", most_recent_action)
                .setParameter("current_activity", current_activity)
                .setParameter("type_of_service_request", type_of_service_request)
                .setParameter("service_request_number", service_request_number)
                .setParameter("completion_date", completion_date)
                .setParameter("status", status)
                .setParameter("creation_date", creation_date)
                .setParameter("number_of_black_carts_delivered", number_of_black_carts_delivered)
                .executeUpdate();

            if (verboseLevel>1) {
                System.out.println("[CustomQuery] Output:");
                System.out.println("\t" + result );
            }
        }
        catch(Exception e) {
            System.err.println("Failed to excecute " + sql + ": " + e.getMessage());
        }

        activeSession.getTransaction().commit();
        return result;
            }

    @SuppressWarnings("unchecked")
    public int addGraffitiRemoval(
            String username,
            Date creation_date,
            String status,
            Date completion_date,
            String service_request_number,
            String type_of_service_request,
            String street_address,
            Integer zip_code,
            Float x_coordinate,
            Float y_coordinate,
            Short ward,
            Short police_district,
            Short community_area,
            Short ssa,
            Float latitude,
            Float longitude,
            String location,
            String what_type_of_surface_is_the_graffiti_on,
            String where_is_the_graffiti_located) {
        String sqlLog = "insert into graffiti_removal_tmp (creation_date, status, completion_date, service_request_number, type_of_service_request, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, ssa, latitude, longitude, location, what_type_of_surface_is_the_graffiti_on, where_is_the_graffiti_located) VALUES ('" + creation_date + "', '" + status + "', '" + completion_date + "', '" + service_request_number + "', '" + type_of_service_request + "', '" + street_address + "', " + zip_code + ", " + x_coordinate + ", " + y_coordinate + ", " + ward + ", " + police_district + ", " + community_area + ", " + ssa + ", " + latitude + ", " + longitude + ", '" + location + "', '" + what_type_of_surface_is_the_graffiti_on + "', '" + where_is_the_graffiti_located + "');";
        queryLogger.logQueryForUser(username, sqlLog);

        activeSession.beginTransaction();

        String sql = "insert into graffiti_removal_tmp (creation_date, status, completion_date, service_request_number, type_of_service_request, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, ssa, latitude, longitude, location, what_type_of_surface_is_the_graffiti_on, where_is_the_graffiti_located) VALUES (:creation_date, :status, :completion_date, :service_request_number, :type_of_service_request, :street_address, :zip_code, :x_coordinate, :y_coordinate, :ward, :police_district, :community_area, :ssa, :latitude, :longitude, :location, :what_type_of_surface_is_the_graffiti_on, :where_is_the_graffiti_located);";

        if (verboseLevel>0) {
            System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
        }

        int result = 0;
        try {
            result = activeSession.createSQLQuery(sql)
                .setParameter("location", location)
                .setParameter("longitude", longitude)
                .setParameter("latitude", latitude)
                .setParameter("ssa", ssa)
                .setParameter("community_area", community_area)
                .setParameter("police_district", police_district)
                .setParameter("ward", ward)
                .setParameter("y_coordinate", y_coordinate)
                .setParameter("x_coordinate", x_coordinate)
                .setParameter("zip_code", zip_code)
                .setParameter("street_address", street_address)
                .setParameter("type_of_service_request", type_of_service_request)
                .setParameter("service_request_number", service_request_number)
                .setParameter("completion_date", completion_date)
                .setParameter("status", status)
                .setParameter("creation_date", creation_date)
                .setParameter("what_type_of_surface_is_the_graffiti_on", what_type_of_surface_is_the_graffiti_on)
                .setParameter("where_is_the_graffiti_located", where_is_the_graffiti_located)
                .executeUpdate();

            if (verboseLevel>1) {
                System.out.println("[CustomQuery] Output:");
                System.out.println("\t" + result );
            }
        }
        catch(Exception e) {
            System.err.println("Failed to excecute " + sql + ": " + e.getMessage());
        }

        activeSession.getTransaction().commit();
        return result;
            }

    @SuppressWarnings("unchecked")
    public int addPotholesReported(
            String username,
            Date creation_date,
            String status,
            Date completion_date,
            String service_request_number,
            String type_of_service_request,
            String current_activity,
            String most_recent_action,
            String street_address,
            Integer zip_code,
            Float x_coordinate,
            Float y_coordinate,
            Short ward,
            Short police_district,
            Short community_area,
            Short ssa,
            Float latitude,
            Float longitude,
            String location,
            Float number_of_potholes_filled_on_block) {
        String sqlLog = "insert into pot_holes_reported(creation_date, status, completion_date, service_request_number, type_of_service_request, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, ssa, latitude, longitude, location, number_of_potholes_filled_on_block) VALUES ('" + creation_date + "', '" + status + "', '" + completion_date + "', '" + service_request_number + "', '" + type_of_service_request + "', '" + current_activity + "', '" + most_recent_action + "', '" + street_address + "', " + zip_code + ", " + x_coordinate + ", " + y_coordinate + ", " + ward + ", " + police_district + ", " + community_area + ", " + ssa + ", " + latitude + ", " + longitude + ", '" + location + "', " + number_of_potholes_filled_on_block + ");";
        queryLogger.logQueryForUser(username, sqlLog);

        activeSession.beginTransaction();

        String sql = "insert into pot_holes_reported(creation_date, status, completion_date, service_request_number, type_of_service_request, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, ssa, latitude, longitude, location, number_of_potholes_filled_on_block) VALUES (:creation_date, :status, :completion_date, :service_request_number, :type_of_service_request, most_recent_action, :street_address, :zip_code, :x_coordinate, :y_coordinate, :ward, :police_district, :community_area, :ssa, :latitude, :longitude, :location, :number_of_potholes_filled_on_block);";

        if (verboseLevel>0) {
            System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
        }

        int result = 0;
        try {
            result = activeSession.createSQLQuery(sql)
                .setParameter("location", location)
                .setParameter("longitude", longitude)
                .setParameter("latitude", latitude)
                .setParameter("ssa", ssa)
                .setParameter("community_area", community_area)
                .setParameter("police_district", police_district)
                .setParameter("ward", ward)
                .setParameter("y_coordinate", y_coordinate)
                .setParameter("x_coordinate", x_coordinate)
                .setParameter("zip_code", zip_code)
                .setParameter("street_address", street_address)
                .setParameter("most_recent_action", most_recent_action)
                .setParameter("current_activity", current_activity)
                .setParameter("type_of_service_request", type_of_service_request)
                .setParameter("service_request_number", service_request_number)
                .setParameter("completion_date", completion_date)
                .setParameter("status", status)
                .setParameter("creation_date", creation_date)
                .setParameter("number_of_potholes_filled_on_block", number_of_potholes_filled_on_block)
                .executeUpdate();

            if (verboseLevel>1) {
                System.out.println("[CustomQuery] Output:");
                System.out.println("\t" + result );
            }
        }
        catch(Exception e) {
            System.err.println("Failed to excecute " + sql + ": " + e.getMessage());
        }

        activeSession.getTransaction().commit();
        return result;
            }

    @SuppressWarnings("unchecked")
    public int addRodentBaiting(
            String username,
            Date creation_date,
            String status,
            Date completion_date,
            String service_request_number,
            String type_of_service_request,
            String current_activity,
            String most_recent_action,
            String street_address,
            Integer zip_code,
            Float x_coordinate,
            Float y_coordinate,
            Short ward,
            Short police_district,
            Short community_area,
            Float latitude,
            Float longitude,
            String location,
            Float number_of_premises_baited,
            Float number_of_premises_with_garbage,
            Float number_of_premises_with_rats) {
                String sqlLog = "insert into rodent_baiting_tmp (creation_date, status, completion_date, service_request_number, type_of_service_request, current_activity, most_recent_action, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, latitude, longitude, location, number_of_premises_baited, number_of_premises_with_garbage, number_of_premises_with_rats) VALUES ('" + creation_date + "', '" + status + "', '" + completion_date + "', '" + service_request_number + "', '" + type_of_service_request + "', '" + current_activity + "', '" + most_recent_action + "', '" + street_address + "', " + zip_code + ", " + x_coordinate + ", " + y_coordinate + ", " + ward + ", " + police_district + ", " + community_area + ", " + latitude + ", " + longitude + ", '" + location + "', " + number_of_premises_baited + ", " + number_of_premises_with_garbage + ", " + number_of_premises_with_rats + ");";
                queryLogger.logQueryForUser(username, sqlLog);
                activeSession.beginTransaction();

                String sql = "insert into rodent_baiting_tmp (creation_date, status, completion_date, service_request_number, type_of_service_request, current_activity, most_recent_action, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, latitude, longitude, location, number_of_premises_baited, number_of_premises_with_garbage, number_of_premises_with_rats) VALUES (:creation_date, :status, :completion_date, :service_request_number, :type_of_service_request, :current_activity, :most_recent_action, :street_address, :zip_code, :x_coordinate, :y_coordinate, :ward, :police_district, :community_area, :latitude, :longitude, :location, :number_of_premises_baited, :number_of_premises_with_garbage, :number_of_premises_with_rats);";

                if (verboseLevel>0) {
                    System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
                }

                int result = 0;
                try {
                    result = activeSession.createSQLQuery(sql)
                        .setParameter("location", location)
                        .setParameter("longitude", longitude)
                        .setParameter("latitude", latitude)
                        .setParameter("community_area", community_area)
                        .setParameter("police_district", police_district)
                        .setParameter("ward", ward)
                        .setParameter("y_coordinate", y_coordinate)
                        .setParameter("x_coordinate", x_coordinate)
                        .setParameter("zip_code", zip_code)
                        .setParameter("street_address", street_address)
                        .setParameter("most_recent_action", most_recent_action)
                        .setParameter("current_activity", current_activity)
                        .setParameter("type_of_service_request", type_of_service_request)
                        .setParameter("service_request_number", service_request_number)
                        .setParameter("completion_date", completion_date)
                        .setParameter("status", status)
                        .setParameter("creation_date", creation_date)
                        .setParameter("number_of_premises_baited", number_of_premises_baited)
                        .setParameter("number_of_premises_with_garbage", number_of_premises_with_garbage)
                        .setParameter("number_of_premises_with_rats", number_of_premises_with_rats)
                        .executeUpdate();

                    if (verboseLevel>1) {
                        System.out.println("[CustomQuery] Output:");
                        System.out.println("\t" + result );
                    }
                }
                catch(Exception e) {
                    System.err.println("Failed to excecute " + sql + ": " + e.getMessage());
                }

                activeSession.getTransaction().commit();
                return result;
            }

    @SuppressWarnings("unchecked")
    public int addSanitationCodeComplaints(
            String username,
            Date creation_date,
            String status,
            Date completion_date,
            String service_request_number,
            String type_of_service_request,
            String street_address,
            Integer zip_code,
            Float x_coordinate,
            Float y_coordinate,
            Short ward,
            Short police_district,
            Short community_area,
            Float latitude,
            Float longitude,
            String location,
            String what_is_the_nature_of_this_code_violation) {
        String sqlLog = "insert into sanitation_code_complaints_tmp (creation_date, status, completion_date, service_request_number, type_of_service_request, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, latitude, longitude, location, what_is_the_nature_of_this_code_violation) VALUES ('" + creation_date + "', '" + status + "', '" + completion_date + "', '" + service_request_number + "', '" + type_of_service_request + "', '" + street_address + "', " + zip_code + ", " + x_coordinate + ", " + y_coordinate + ", " + ward + ", " + police_district + ", " + community_area + ", " + latitude + ", " + longitude + ", '" + location + "', '" + what_is_the_nature_of_this_code_violation + "');";
        queryLogger.logQueryForUser(username, sqlLog);

        activeSession.beginTransaction();

        String sql = "insert into sanitation_code_complaints_tmp (creation_date, status, completion_date, service_request_number, type_of_service_request, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, latitude, longitude, location, what_is_the_nature_of_this_code_violation) VALUES (:creation_date, :status, :completion_date, :service_request_number, :type_of_service_request, :street_address, :zip_code, :x_coordinate, :y_coordinate, :ward, :police_district, :community_area, :latitude, :longitude, :location, :what_is_the_nature_of_this_code_violation);";

        if (verboseLevel>0) {
            System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
        }

        int result = 0;
        try {
            result = activeSession.createSQLQuery(sql)
                .setParameter("location", location)
                .setParameter("longitude", longitude)
                .setParameter("latitude", latitude)
                .setParameter("community_area", community_area)
                .setParameter("police_district", police_district)
                .setParameter("ward", ward)
                .setParameter("y_coordinate", y_coordinate)
                .setParameter("x_coordinate", x_coordinate)
                .setParameter("zip_code", zip_code)
                .setParameter("street_address", street_address)
                .setParameter("type_of_service_request", type_of_service_request)
                .setParameter("service_request_number", service_request_number)
                .setParameter("completion_date", completion_date)
                .setParameter("status", status)
                .setParameter("creation_date", creation_date)
                .setParameter("what_is_the_nature_of_this_code_violation", what_is_the_nature_of_this_code_violation)

                .executeUpdate();

            if (verboseLevel>1) {
                System.out.println("[CustomQuery] Output:");
                System.out.println("\t" + result );
            }
        }
        catch(Exception e) {
            System.err.println("Failed to excecute " + sql + ": " + e.getMessage());
        }

        activeSession.getTransaction().commit();
        return result;
            }

    @SuppressWarnings("unchecked")
    public int addStreetLightsAllOut(
            String username,
            Date creation_date,
            String status,
            Date completion_date,
            String service_request_number,
            String type_of_service_request,
            String street_address,
            Integer zip_code,
            Float x_coordinate,
            Float y_coordinate,
            Short ward,
            Short police_district,
            Short community_area,
            Float latitude,
            Float longitude,
            String location) {
        String sqlLog = "insert into street_lights_all_out_tmp (creation_date, status, completion_date, service_request_number, type_of_service_request, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, latitude, longitude, location) VALUES ('" + creation_date + "', '" + status + "', '" + completion_date + "', '" + service_request_number + "', '" + type_of_service_request + "', '" + street_address + "', " + zip_code + ", " + x_coordinate + ", " + y_coordinate + ", " + ward + ", " + police_district + ", " + community_area + ", " + latitude + ", " + longitude + ", '" + location + "');";
        queryLogger.logQueryForUser(username, sqlLog);

        activeSession.beginTransaction();

        String sql = "insert into street_lights_all_out_tmp (creation_date, status, completion_date, service_request_number, type_of_service_request, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, latitude, longitude, location) VALUES (:creation_date, :status, :completion_date, :service_request_number, :type_of_service_request, :street_address, :zip_code, :x_coordinate, :y_coordinate, :ward, :police_district, :community_area, :latitude, :longitude, :location);";

        if (verboseLevel>0) {
            System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
        }

        int result = 0;
        try {
            result = activeSession.createSQLQuery(sql)
                .setParameter("location", location)
                .setParameter("longitude", longitude)
                .setParameter("latitude", latitude)
                .setParameter("community_area", community_area)
                .setParameter("police_district", police_district)
                .setParameter("ward", ward)
                .setParameter("y_coordinate", y_coordinate)
                .setParameter("x_coordinate", x_coordinate)
                .setParameter("zip_code", zip_code)
                .setParameter("street_address", street_address)
                .setParameter("type_of_service_request", type_of_service_request)
                .setParameter("service_request_number", service_request_number)
                .setParameter("completion_date", completion_date)
                .setParameter("status", status)
                .setParameter("creation_date", creation_date)
                .executeUpdate();

            if (verboseLevel>1) {
                System.out.println("[CustomQuery] Output:");
                System.out.println("\t" + result );
            }
        }
        catch(Exception e) {
            System.err.println("Failed to excecute " + sql + ": " + e.getMessage());
        }

        activeSession.getTransaction().commit();
        return result;
            }


    @SuppressWarnings("unchecked")
    public int addStreetLightsOneOut(
            String username,
            Date creation_date,
            String status,
            Date completion_date,
            String service_request_number,
            String type_of_service_request,
            String street_address,
            Integer zip_code,
            Float x_coordinate,
            Float y_coordinate,
            Short ward,
            Short police_district,
            Short community_area,
            Float latitude,
            Float longitude,
            String location) {

        String sqlLog = "insert into street_lights_one_out_tmp (creation_date, status, completion_date, service_request_number, type_of_service_request, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, latitude, longitude, location) VALUES ('" + creation_date + "', '" + status + "', '" + completion_date + "', '" + service_request_number + "', '" + type_of_service_request + "', '" + street_address + "', " + zip_code + ", " + x_coordinate + ", " + y_coordinate + ", " + ward + ", " + police_district + ", " + community_area + ", " + latitude + ", " + longitude + ", '" + location + "');";
        queryLogger.logQueryForUser(username, sqlLog);

        activeSession.beginTransaction();

        String sql = "insert into street_lights_one_out_tmp (creation_date, status, completion_date, service_request_number, type_of_service_request, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, latitude, longitude, location) VALUES (:creation_date, :status, :completion_date, :service_request_number, :type_of_service_request, :street_address, :zip_code, :x_coordinate, :y_coordinate, :ward, :police_district, :community_area, :latitude, :longitude, :location);";

        if (verboseLevel>0) {
            System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
        }

        int result = 0;
        try {
            result = activeSession.createSQLQuery(sql)
                .setParameter("location", location)
                .setParameter("longitude", longitude)
                .setParameter("latitude", latitude)
                .setParameter("community_area", community_area)
                .setParameter("police_district", police_district)
                .setParameter("ward", ward)
                .setParameter("y_coordinate", y_coordinate)
                .setParameter("x_coordinate", x_coordinate)
                .setParameter("zip_code", zip_code)
                .setParameter("street_address", street_address)
                .setParameter("type_of_service_request", type_of_service_request)
                .setParameter("service_request_number", service_request_number)
                .setParameter("completion_date", completion_date)
                .setParameter("status", status)
                .setParameter("creation_date", creation_date)
                .executeUpdate();

            if (verboseLevel>1) {
                System.out.println("[CustomQuery] Output:");
                System.out.println("\t" + result );
            }
        }
        catch(Exception e) {
            System.err.println("Failed to excecute " + sql + ": " + e.getMessage());
        }

        activeSession.getTransaction().commit();
        return result;
            }

    @SuppressWarnings("unchecked")
    public int addTreeDebris(
            String username,
            Date creation_date,
            String status,
            Date completion_date,
            String service_request_number,
            String type_of_service_request,
            String current_activity,
            String most_recent_action,
            String street_address,
            Integer zip_code,
            Float x_coordinate,
            Float y_coordinate,
            Short ward,
            Short police_district,
            Short community_area,
            Float latitude,
            Float longitude,
            String location,
            String if_yes_where_is_the_debris_located) {
        String sqlLog = "insert into tree_debris_tmp (creation_date, status, completion_date, service_request_number, type_of_service_request, current_activity, most_recent_action, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, latitude, longitude, location, if_yes_where_is_the_debris_located) VALUES ('" + creation_date + "', '" + status + "', '" + completion_date + "', '" + service_request_number + "', '" + type_of_service_request + "', '" + current_activity + "', '" + most_recent_action + "', '" + street_address + "', " + zip_code + ", " + x_coordinate + ", " + y_coordinate + ", " + ward + ", " + police_district + ", " + community_area + ", " + latitude + ", " + longitude + ", '" + location + "', '" + if_yes_where_is_the_debris_located + "');";
        queryLogger.logQueryForUser(username, sqlLog);

        activeSession.beginTransaction();

        String sql = "insert into tree_debris_tmp (creation_date, status, completion_date, service_request_number, type_of_service_request, current_activity, most_recent_action, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, latitude, longitude, location, if_yes_where_is_the_debris_located) VALUES (:creation_date, :status, :completion_date, :service_request_number, :type_of_service_request, :current_activity, :most_recent_action, :street_address, :zip_code, :x_coordinate, :y_coordinate, :ward, :police_district, :community_area, :latitude, :longitude, :location, :if_yes_where_is_the_debris_located);";

        if (verboseLevel>0) {
            System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
        }

        int result = 0;
        try {
            result = activeSession.createSQLQuery(sql)
                .setParameter("location", location)
                .setParameter("longitude", longitude)
                .setParameter("latitude", latitude)
                .setParameter("community_area", community_area)
                .setParameter("police_district", police_district)
                .setParameter("ward", ward)
                .setParameter("y_coordinate", y_coordinate)
                .setParameter("x_coordinate", x_coordinate)
                .setParameter("zip_code", zip_code)
                .setParameter("street_address", street_address)
                .setParameter("most_recent_action", most_recent_action)
                .setParameter("current_activity", current_activity)
                .setParameter("type_of_service_request", type_of_service_request)
                .setParameter("service_request_number", service_request_number)
                .setParameter("completion_date", completion_date)
                .setParameter("status", status)
                .setParameter("creation_date", creation_date)
                .setParameter("if_yes_where_is_the_debris_located", if_yes_where_is_the_debris_located)
                .executeUpdate();

            if (verboseLevel>1) {
                System.out.println("[CustomQuery] Output:");
                System.out.println("\t" + result );
            }
        }
        catch(Exception e) {
            System.err.println("Failed to excecute " + sql + ": " + e.getMessage());
        }

        activeSession.getTransaction().commit();
        return result;
            }

    @SuppressWarnings("unchecked")
    public int addTreeTrims(
            String username,
            Date creation_date,
            String status,
            Date completion_date,
            String service_request_number,
            String type_of_service_request,
            String street_address,
            Integer zip_code,
            Float x_coordinate,
            Float y_coordinate,
            Short ward,
            Short police_district,
            Short community_area,
            Float latitude,
            Float longitude,
            String location,
            String location_of_trees) {
        String sqlLog = "insert into tree_trims_tmp (creation_date, status, completion_date, service_request_number, type_of_service_request, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, latitude, longitude, location, location_of_trees)  VALUES ('" + creation_date + "', '" + status + "', '" + completion_date + "', '" + service_request_number + "', '" + type_of_service_request + "', '" + street_address + "', " + zip_code + ", " + x_coordinate + ", " + y_coordinate + ", " + ward + ", " + police_district + ", " + community_area + ", " + latitude + ", " + longitude + ", '" + location + "', '" + location_of_trees + "');";
        queryLogger.logQueryForUser(username, sqlLog);

        activeSession.beginTransaction();

        String sql = "insert into tree_trims_tmp (creation_date, status, completion_date, service_request_number, type_of_service_request, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, latitude, longitude, location, location_of_trees)  VALUES (:creation_date, :status, :completion_date, :service_request_number, :type_of_service_request, :street_address, :zip_code, :x_coordinate, :y_coordinate, :ward, :police_district, :community_area, :latitude, :longitude, :location, :location_of_trees);";


        if (verboseLevel>0) {
            System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
        }

        int result = 0;
        try {
            result = activeSession.createSQLQuery(sql)
                .setParameter("location", location)
                .setParameter("longitude", longitude)
                .setParameter("latitude", latitude)
                .setParameter("community_area", community_area)
                .setParameter("police_district", police_district)
                .setParameter("ward", ward)
                .setParameter("y_coordinate", y_coordinate)
                .setParameter("x_coordinate", x_coordinate)
                .setParameter("zip_code", zip_code)
                .setParameter("street_address", street_address)
                .setParameter("type_of_service_request", type_of_service_request)
                .setParameter("service_request_number", service_request_number)
                .setParameter("completion_date", completion_date)
                .setParameter("status", status)
                .setParameter("creation_date", creation_date)
                .setParameter("location_of_trees", location_of_trees)
                .executeUpdate();

            if (verboseLevel>1) {
                System.out.println("[CustomQuery] Output:");
                System.out.println("\t" + result );
            }
        }
        catch(Exception e) {
            System.err.println("Failed to excecute " + sql + ": " + e.getMessage());
        }

        activeSession.getTransaction().commit();
        return result;
            }

}
