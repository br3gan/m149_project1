package di.postgrad.databasesystems.project1.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the garbage_carts database table.
 * 
 */
@Entity
@Table(name="garbage_carts")
public class GarbageCart implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name="number_of_black_carts_delivered")
	private Float numberOfBlackCartsDelivered;

	//bi-directional many-to-one association to Action
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="actionsid", nullable = false)
	private Action action;

	//bi-directional many-to-one association to Status
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="statusid", nullable = false)
	private Status status;

	public GarbageCart() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Float getNumberOfBlackCartsDelivered() {
		return this.numberOfBlackCartsDelivered;
	}

	public void setNumberOfBlackCartsDelivered(Float numberOfBlackCartsDelivered) {
		this.numberOfBlackCartsDelivered = numberOfBlackCartsDelivered;
	}

	public Action getAction() {
		return this.action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	public Status getStatus() {
		return this.status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

}