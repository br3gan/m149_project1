package di.postgrad.databasesystems.project1.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the pot_holes_reported database table.
 * 
 */
@Entity
@Table(name="pot_holes_reported")
public class PotHolesReported implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name="number_of_potholes_filled_on_block")
	private Float numberOfPotholesFilledOnBlock;

	//bi-directional many-to-one association to Action
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="actionsid", nullable = false)
	private Action action;

	//bi-directional many-to-one association to Status
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="statusid", nullable = false)
	private Status status;

	public PotHolesReported() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Float getNumberOfPotholesFilledOnBlock() {
		return this.numberOfPotholesFilledOnBlock;
	}

	public void setNumberOfPotholesFilledOnBlock(Float numberOfPotholesFilledOnBlock) {
		this.numberOfPotholesFilledOnBlock = numberOfPotholesFilledOnBlock;
	}

	public Action getAction() {
		return this.action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	public Status getStatus() {
		return this.status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

}