package di.postgrad.databasesystems.project1.service;


import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import di.postgrad.databasesystems.project1.domain.SanitationCodeComplaint;
import di.postgrad.databasesystems.project1.repository.SanitationCodeComplaintRepository;

@Service
public class SanitationCodeComplaintService {

    @Autowired
    SanitationCodeComplaintRepository sanitationCodeComplaintRepository;

    @Autowired
    LocationSpecificRequestsService locationSpecificRequestsService;

    public SanitationCodeComplaint addSanitationCodeComplaint(SanitationCodeComplaint sanitationCodeComplaint) {
        sanitationCodeComplaintRepository.save(sanitationCodeComplaint);
        return sanitationCodeComplaint;
    }

    public Iterable<SanitationCodeComplaint> getSanitationCodeComplaints(){
        return sanitationCodeComplaintRepository.findAll();
    }

    public SanitationCodeComplaint getSanitationCodeComplaint(Long sanitationCodeComplaintId){
      return sanitationCodeComplaintRepository.findOne(sanitationCodeComplaintId);
  }

    public Iterable<SanitationCodeComplaint> findLocationSpecificSanitationCodeComplaints(String username, String street_address, Integer zip_code){

        Iterable<BigInteger> sanitation_code_complaints_id = locationSpecificRequestsService.findLocationSpecificSanitationCodeComplaints(username, street_address, zip_code);
        List<SanitationCodeComplaint> sanitation_code_complaint_list = new ArrayList<>();
        if (sanitation_code_complaints_id != null) {
        	for (BigInteger id : sanitation_code_complaints_id) {
            	sanitation_code_complaint_list.add(sanitationCodeComplaintRepository.findOne(id.longValue()));
            }
        }
        
        return (Iterable<SanitationCodeComplaint>) sanitation_code_complaint_list;
    }
}
