package di.postgrad.databasesystems.project1.service;

import java.util.Date;

import org.hibernate.Session;
import org.springframework.stereotype.Service;

import di.postgrad.databasesystems.project1.customQuery.CustomQuerySessionGenerator;
import di.postgrad.databasesystems.project1.customQuery.StoredFunctionsRetriever;

@Service
public class  StoredFunctionsService{

	private final Session session = CustomQuerySessionGenerator.getSessionFactory().openSession();
	private final StoredFunctionsRetriever sfr = new StoredFunctionsRetriever(session, 2);

	@Override
	protected void finalize() {
		CustomQuerySessionGenerator.shutdown();
		session.close();
	}

    public Iterable<Object[]> getFunction01(Date startingDate, Date endingDate, String username){
        Iterable<Object[]> result = sfr.getFunction01(startingDate, endingDate, username);
    	return result;
    }

    public Iterable<Object[]> getFunction02(String typeOfServiceRequest, Date startingDate, Date endingDate, String username){
        Iterable<Object[]> result = sfr.getFunction02(typeOfServiceRequest, startingDate, endingDate, username);
    	return result;
    }
    
    public Iterable<Object[]> getFunction03(Date creationDate, String username){
        Iterable<Object[]> result = sfr.getFunction03(creationDate, username);
    	return result;
    }

    public Iterable<Object[]> getFunction04(Date startingDate, Date endingDate, String username){
        Iterable<Object[]> result = sfr.getFunction04(startingDate, endingDate, username);
    	return result;
    }

    public Iterable<Object[]> getFunction05(Float min_lon, Float max_lon, Float min_lat, Float max_lat, Date creationDate, String username){
        Iterable<Object[]> result = sfr.getFunction05(min_lon, max_lon, min_lat, max_lat, creationDate, username);
    	return result;
    }

    public Iterable<Object[]> getFunction06(Date startingDate, Date endingDate, String username){
        Iterable<Object[]> result = sfr.getFunction06(startingDate, endingDate, username);
    	return result;
    }

    public Iterable<Object[]> getFunction07(String username){
        Iterable<Object[]> result = sfr.getFunction07(username);
    	return result;
    }
    
    public Iterable<Object[]> getFunction08(String username){
        Iterable<Object[]> result = sfr.getFunction08(username);
    	return result;
    }

    public Iterable<Object[]> getFunction09(Float maxNumberOfPremisesBaited, String username){
        Iterable<Object[]> result = sfr.getFunction09(maxNumberOfPremisesBaited, username);
    	return result;
    }
    
    public Iterable<Object[]> getFunction10(Float maxNumberOfPremisesWithGarbage, String username){
        Iterable<Object[]> result = sfr.getFunction10(maxNumberOfPremisesWithGarbage, username);
    	return result;
    }

    public Iterable<Object[]> getFunction11(Float maxNumberOfPremisesWithRats, String username){
        Iterable<Object[]> result = sfr.getFunction10(maxNumberOfPremisesWithRats, username);
    	return result;
    }

    public Iterable<Object> getFunction12(Float maxNumberOfPotholesFilledOnBlock, Float numberOfPremisesBaited, Date competionDate, String username){
        Iterable<Object> result = sfr.getFunction12(maxNumberOfPotholesFilledOnBlock, numberOfPremisesBaited, competionDate, username);
    	return result;
    }
    
}
    
