package di.postgrad.databasesystems.project1.service;


import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import di.postgrad.databasesystems.project1.domain.RodentBaiting;
import di.postgrad.databasesystems.project1.repository.RodentBaitingRepository;

@Service
public class RodentBaitingService {

    @Autowired
    RodentBaitingRepository rodentBaitingRepository;

    @Autowired
    LocationSpecificRequestsService locationSpecificRequestsService;

    public RodentBaiting addRodentBaiting(RodentBaiting rodentBaiting) {
        rodentBaitingRepository.save(rodentBaiting);
        return rodentBaiting;
    }

    public Iterable<RodentBaiting> getRodentBaitings(){
        return rodentBaitingRepository.findAll();
    }

    public RodentBaiting getRodentBaiting(Long rodentBaitingId){
      return rodentBaitingRepository.findOne(rodentBaitingId);
  }

    public Iterable<RodentBaiting> findLocationSpecificRodentBaiting(String username, String street_address, Integer zip_code){

        Iterable<BigInteger> rodent_baitings_id = locationSpecificRequestsService.findLocationSpecificRodentBaiting(username, street_address, zip_code);
        List<RodentBaiting> rodent_baiting_list = new ArrayList<>();
        if (rodent_baitings_id != null) {
        	for (BigInteger id : rodent_baitings_id) {
            	rodent_baiting_list.add(rodentBaitingRepository.findOne(id.longValue()));
            }
        }
        
        return (Iterable<RodentBaiting>) rodent_baiting_list;
    }
}
