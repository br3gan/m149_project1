package di.postgrad.databasesystems.project1.security;

import java.util.ArrayList;
import java.util.List;

import di.postgrad.databasesystems.project1.customQuery.CustomQuerySessionGenerator;
import di.postgrad.databasesystems.project1.domain.AppUser;
import di.postgrad.databasesystems.project1.customQuery.Authenticator;

import org.hibernate.Session;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
 
@Service
public class UserDetailsServiceImpl implements UserDetailsService {
 
   // @Autowired
    ///private AppUser appUser = new AppUser();
 
    private final Session session = CustomQuerySessionGenerator.getSessionFactory().openSession();
	private final Authenticator authenticator = new Authenticator(session, 2);

	@Override
	protected void finalize() {
		CustomQuerySessionGenerator.shutdown();
		session.close();
	}
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AppUser appUser = authenticator.findUserAccount(username);
 
        if (appUser == null) {
            System.out.println("User not found! " + username);
            throw new UsernameNotFoundException("User " + username + " was not found in the database");
        }
 
        System.out.println("Found User: " + appUser);
 
        // [USER, ADMIN,..]
 
        List<GrantedAuthority> grantList = new ArrayList<GrantedAuthority>();

        GrantedAuthority authority = new SimpleGrantedAuthority(appUser.getRole());
        grantList.add(authority);
 
        UserDetails userDetails = (UserDetails) new User(appUser.getUsername(), //
                appUser.getEncryptedPassword(), grantList);
 
        return userDetails;
    }
    
    public AppUser findAppUserByUsername(String username)  {
    	return authenticator.findUserAccount(username);
    }
    
    public Integer addAppUser(String username, String encrypted_password, String address, String email) {
    	return authenticator.addAppUser(username, encrypted_password, address, email);
    }
 
}