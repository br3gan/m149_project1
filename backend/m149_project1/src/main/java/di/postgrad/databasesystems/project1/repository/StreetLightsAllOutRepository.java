package di.postgrad.databasesystems.project1.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import di.postgrad.databasesystems.project1.domain.StreetLightsAllOut;

@Repository
public interface StreetLightsAllOutRepository extends CrudRepository<StreetLightsAllOut, Long> {

}