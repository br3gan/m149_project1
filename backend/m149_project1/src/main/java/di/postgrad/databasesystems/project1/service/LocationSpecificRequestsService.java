package di.postgrad.databasesystems.project1.service;

import java.math.BigInteger;

import org.hibernate.Session;
import org.springframework.stereotype.Service;

import di.postgrad.databasesystems.project1.customQuery.CustomQuerySessionGenerator;
import di.postgrad.databasesystems.project1.customQuery.LocationSpecificRequestsRetriever;

@Service
public class LocationSpecificRequestsService {

	private final Session session = CustomQuerySessionGenerator.getSessionFactory().openSession();
	private final LocationSpecificRequestsRetriever lsrr = new LocationSpecificRequestsRetriever(session, 2);

	@Override
	protected void finalize() {
		CustomQuerySessionGenerator.shutdown();
		session.close();
	}
	
	public Iterable<BigInteger> findLocationSpecificAbandonedVehicles(String username, String street, Integer zip_code ){
        Iterable<BigInteger> result = lsrr.findLocationSpecificAbandonedVehicles(username, street, zip_code);
    	return result;
    }
	
	public Iterable<BigInteger> findLocationSpecificGarbageCarts(String username, String street, Integer zip_code ){
        Iterable<BigInteger> result = lsrr.findLocationSpecificGarbageCarts(username, street, zip_code);
    	return result;
    }
	
	public Iterable<BigInteger> findLocationSpecificGraffitiRemoval(String username, String street, Integer zip_code ){
        Iterable<BigInteger> result = lsrr.findLocationSpecificGraffitiRemoval(username, street, zip_code);
    	return result;
    }
	
	public Iterable<BigInteger> findLocationSpecificPotHolesReported(String username, String street, Integer zip_code ){
        Iterable<BigInteger> result = lsrr.findLocationSpecificPotHolesReported(username, street, zip_code);
    	return result;
    }
	
	public Iterable<BigInteger> findLocationSpecificRodentBaiting(String username, String street, Integer zip_code ){
        Iterable<BigInteger> result = lsrr.findLocationSpecificRodentBaiting(username, street, zip_code);
    	return result;
    }
	
	public Iterable<BigInteger> findLocationSpecificSanitationCodeComplaints(String username, String street, Integer zip_code ){
        Iterable<BigInteger> result = lsrr.findLocationSpecificSanitationCodeComplaints(username, street, zip_code);
    	return result;
    }
	
	public Iterable<BigInteger> findLocationSpecificTreeDebris(String username, String street, Integer zip_code ){
        Iterable<BigInteger> result = lsrr.findLocationSpecificTreeDebris(username, street, zip_code);
    	return result;
    }
	
	public Iterable<BigInteger> findLocationSpecificTreeTrims(String username, String street, Integer zip_code ){
        Iterable<BigInteger> result = lsrr.findLocationSpecificTreeTrims(username, street, zip_code);
    	return result;
    }
	
	public Iterable<BigInteger> findLocationSpecificAlleyLightsOut(String username, String street, Integer zip_code ){
        Iterable<BigInteger> result = lsrr.findLocationSpecificAlleyLightsOut(username, street, zip_code);
    	return result;
    }
	
	public Iterable<BigInteger> findLocationSpecificStreetLightsAllOut(String username, String street, Integer zip_code ){
        Iterable<BigInteger> result = lsrr.findLocationSpecificStreetLightsAllOut(username, street, zip_code);
    	return result;
    }
	
	public Iterable<BigInteger> findLocationSpecificStreetLightsOneOut(String username, String street, Integer zip_code ){
        Iterable<BigInteger> result = lsrr.findLocationSpecificStreetLightsOneOut(username, street, zip_code);
    	return result;
    }
}
