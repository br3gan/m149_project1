package di.postgrad.databasesystems.project1.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the location database table.
 * 
 */
@Entity
@NamedQuery(name="Location.findAll", query="SELECT l FROM Location l")
public class Location implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long locationid;

	@Column(name="community_area")
	private Integer communityArea;

	private Double latitude;

	private String location;

	private Double longitude;

	@Column(name="police_district")
	private Integer policeDistrict;

	private Integer ssa;

	@Column(name="street_address")
	private String streetAddress;

	private Integer ward;

	@Column(name="x_coordinate")
	private Double xCoordinate;

	@Column(name="y_coordinate")
	private Double yCoordinate;

	@Column(name="zip_code")
	private Integer zipCode;

	//bi-directional many-to-one association to Status
	@OneToMany(fetch = FetchType.LAZY, mappedBy="location")
	private List<Status> statuses;

	public Location() {
	}

	public Long getLocationid() {
		return this.locationid;
	}

	public void setLocationid(Long locationid) {
		this.locationid = locationid;
	}

	public Integer getCommunityArea() {
		return this.communityArea;
	}

	public void setCommunityArea(Integer communityArea) {
		this.communityArea = communityArea;
	}

	public Double getLatitude() {
		return this.latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Double getLongitude() {
		return this.longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Integer getPoliceDistrict() {
		return this.policeDistrict;
	}

	public void setPoliceDistrict(Integer policeDistrict) {
		this.policeDistrict = policeDistrict;
	}

	public Integer getSsa() {
		return this.ssa;
	}

	public void setSsa(Integer ssa) {
		this.ssa = ssa;
	}

	public String getStreetAddress() {
		return this.streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public Integer getWard() {
		return this.ward;
	}

	public void setWard(Integer ward) {
		this.ward = ward;
	}

	public Double getXCoordinate() {
		return this.xCoordinate;
	}

	public void setXCoordinate(Double xCoordinate) {
		this.xCoordinate = xCoordinate;
	}

	public Double getYCoordinate() {
		return this.yCoordinate;
	}

	public void setYCoordinate(Double yCoordinate) {
		this.yCoordinate = yCoordinate;
	}

	public Integer getZipCode() {
		return this.zipCode;
	}

	public void setZipCode(Integer zipCode) {
		this.zipCode = zipCode;
	}

	public List<Status> getStatuses() {
		return this.statuses;
	}

	public void setStatuses(List<Status> statuses) {
		this.statuses = statuses;
	}

	public Status addStatus(Status status) {
		getStatuses().add(status);
		status.setLocation(this);

		return status;
	}

	public Status removeStatus(Status status) {
		getStatuses().remove(status);
		status.setLocation(null);

		return status;
	}

}