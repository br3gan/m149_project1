package di.postgrad.databasesystems.project1.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import di.postgrad.databasesystems.project1.domain.GarbageCart;

@Repository
public interface GarbageCartRepository extends CrudRepository<GarbageCart, Long> {

}

