package di.postgrad.databasesystems.project1.domain;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
 
@Entity
@Table(name = "app_user", //
        uniqueConstraints = { //
                @UniqueConstraint(name = "APP_USER_UK", columnNames = "username") })
public class AppUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @Column(name = "username", nullable = false)
    private String username;
 
    @Column(name = "encrypted_password", length = 128, nullable = false)
    private String encryptedPassword;
    
    @Column(name = "address", length = 128, nullable = false)
    private String address;
 
    @Column(name = "email", length = 64, nullable = false)
    private String email;
    
    @Column(name = "enabled", length = 1, nullable = false)
    private Boolean enabled;
    
    @Column(name = "role", nullable = false)
    private String role;
    
    public AppUser() {};
 
	public String getUsername() {
        return username;
    }
 
    public void setUsername(String username) {
        this.username = username;
    }
 
    public String getEncryptedPassword() {
        return encryptedPassword;
    }
 
    public void setEncryptedPassword(String encryptedPassword) {
        this.encryptedPassword = encryptedPassword;
    }
 
    public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean isEnabled() {
        return enabled;
    }
 
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
    
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
 
}
