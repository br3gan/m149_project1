package di.postgrad.databasesystems.project1.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the tree_debris database table.
 * 
 */
@Entity
@Table(name="tree_debris")
public class TreeDebri implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name="if_yes_where_is_the_debris_located")
	private String ifYesWhereIsTheDebrisLocated;

	//bi-directional many-to-one association to Action
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="actionsid", nullable = false)
	private Action action;

	//bi-directional many-to-one association to Status
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="statusid", nullable = false)
	private Status status;

	public TreeDebri() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIfYesWhereIsTheDebrisLocated() {
		return this.ifYesWhereIsTheDebrisLocated;
	}

	public void setIfYesWhereIsTheDebrisLocated(String ifYesWhereIsTheDebrisLocated) {
		this.ifYesWhereIsTheDebrisLocated = ifYesWhereIsTheDebrisLocated;
	}

	public Action getAction() {
		return this.action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	public Status getStatus() {
		return this.status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

}