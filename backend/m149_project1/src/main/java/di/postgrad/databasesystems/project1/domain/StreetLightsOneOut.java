package di.postgrad.databasesystems.project1.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the street_lights_one_out database table.
 * 
 */
@Entity
@Table(name="street_lights_one_out")
public class StreetLightsOneOut implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	//bi-directional many-to-one association to Status
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="statusid", nullable = false)
	private Status status;

	public StreetLightsOneOut() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Status getStatus() {
		return this.status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

}