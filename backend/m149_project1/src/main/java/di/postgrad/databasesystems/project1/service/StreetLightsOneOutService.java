package di.postgrad.databasesystems.project1.service;


import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import di.postgrad.databasesystems.project1.domain.StreetLightsOneOut;
import di.postgrad.databasesystems.project1.repository.StreetLightsOneOutRepository;

@Service
public class StreetLightsOneOutService {

    @Autowired
    StreetLightsOneOutRepository streetLightsOneOutRepository;
    
    @Autowired
    LocationSpecificRequestsService locationSpecificRequestsService;

    public StreetLightsOneOut addStreetLightsOneOut(StreetLightsOneOut streetLightsOneOut) {
        streetLightsOneOutRepository.save(streetLightsOneOut);
        return streetLightsOneOut;
    }

    public Iterable<StreetLightsOneOut> getStreetLightsOneOuts(){
        return streetLightsOneOutRepository.findAll();
    }

    public StreetLightsOneOut getStreetLightsOneOut(Long streetLightsOneOutId){
      return streetLightsOneOutRepository.findOne(streetLightsOneOutId);
  }
    
    public Iterable<StreetLightsOneOut> findLocationSpecificStreetLightsOneOut(String username, String street_address, Integer zip_code){
    	
        Iterable<BigInteger> street_lights_one_outs_id = locationSpecificRequestsService.findLocationSpecificStreetLightsOneOut(username, street_address, zip_code);
        List<StreetLightsOneOut> street_lights_one_out_list = new ArrayList<>();
        if (street_lights_one_outs_id != null) {
        	for (BigInteger id : street_lights_one_outs_id) {
            	street_lights_one_out_list.add(streetLightsOneOutRepository.findOne(id.longValue()));
            }
        }
        
        return (Iterable<StreetLightsOneOut>) street_lights_one_out_list;
    }
}