package di.postgrad.databasesystems.project1.customQuery;


import org.hibernate.Session;

import di.postgrad.databasesystems.project1.domain.AppUser;

public class Authenticator {

	private Session activeSession;
	private int verboseLevel;

	public Authenticator(Session _activeSession, int _verboseLevel) {
		activeSession = _activeSession;
		verboseLevel = _verboseLevel;
	}
	
	/**
	 * 
	 * @param username
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public AppUser findUserAccount(String username) {
        activeSession.beginTransaction();
        
        String sql = "SELECT * FROM app_user where username='" + username + "';";

        if (verboseLevel>0) {
        	System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
        }

        Iterable<Object[]> result = null;
        try {
        	result = (Iterable<Object[]>) activeSession.createSQLQuery(sql)
        									.list();
        	

        	if (verboseLevel>1) {
        		System.out.println("[CustomQuery] Output:");
        		for(Object[] r : result){
       				System.out.println("\t" + r[0]);
        		}
        	}
        }
        catch(Exception e) {
        	System.err.println("Failed to excecute " + sql + ": " + e.getMessage());
        }

        
        AppUser app_user = null; 
        if (result.iterator().hasNext()) {
        	Object[] temp = result.iterator().next();
        	app_user = new AppUser();
            app_user.setUsername((String)temp[0]);
            app_user.setEncryptedPassword((String) temp[1]);
            app_user.setAddress((String) temp[2]);
            app_user.setEmail((String) temp[3]);
            app_user.setRole((String) temp[4]);
            app_user.setEnabled((Boolean) temp[5]);
        }
        
    	activeSession.getTransaction().commit();

        return app_user ;
	}
	
	@SuppressWarnings("unchecked")
	public Integer addAppUser(String username, String encrypted_password, String address, String email) {
        activeSession.beginTransaction();
        
        String sql = "INSERT INTO app_user VALUES('" + username + "', '" + encrypted_password + "', '" + address + "', '"   + email + "', 'USER', 'false')";

        if (verboseLevel>0) {
        	System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
        }

        Integer result = null;
        try {
        	result = activeSession.createSQLQuery(sql)
        									.executeUpdate();
        	

        	if (verboseLevel>1) {
        		System.out.println("[CustomQuery] Output:" + result);
        	}
        }
        catch(Exception e) {
        	System.err.println("Failed to excecute " + sql + ": " + e.getMessage());
        }

        activeSession.getTransaction().commit();
        
        return result ;
	}
}
