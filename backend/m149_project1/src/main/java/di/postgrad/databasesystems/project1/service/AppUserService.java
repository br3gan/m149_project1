package di.postgrad.databasesystems.project1.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import di.postgrad.databasesystems.project1.domain.AppUser;
import di.postgrad.databasesystems.project1.repository.AppUserRepository;

@Service
public class AppUserService {

    @Autowired
    AppUserRepository appUserRepository;

    public AppUser addAppUser(AppUser appUser) {
        appUserRepository.save(appUser);
        return appUser;
    }

    public Iterable<AppUser> getAppUsers(){
        return appUserRepository.findAll();
    }

    public AppUser getAppUser(String username){
      return appUserRepository.findOne(username);
    }
}