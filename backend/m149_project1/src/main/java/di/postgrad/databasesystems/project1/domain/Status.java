package di.postgrad.databasesystems.project1.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the status database table.
 *
 */

@Entity
public class Status implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long statusid;

	@Temporal(TemporalType.DATE)
	@Column(name="completion_date")
	private Date completionDate;

	@Temporal(TemporalType.DATE)
	@Column(name="creation_date")
	private Date creationDate;

	@Column(name="service_request_number")
	private String serviceRequestNumber;

	private String status;

	//bi-directional many-to-one association to AbandonedVehicle
	@OneToMany(fetch = FetchType.LAZY, mappedBy="status")
	private List<AbandonedVehicle> abandonedVehicles;

	//bi-directional many-to-one association to AlleyLightsOut
	@OneToMany(fetch = FetchType.LAZY, mappedBy="status")
	private List<AlleyLightsOut> alleyLightsOuts;

	//bi-directional many-to-one association to GarbageCart
	@OneToMany(fetch = FetchType.LAZY, mappedBy="status")
	private List<GarbageCart> garbageCarts;

	//bi-directional many-to-one association to GraffitiRemoval
	@OneToMany(fetch = FetchType.LAZY, mappedBy="status")
	private List<GraffitiRemoval> graffitiRemovals;

	//bi-directional many-to-one association to PotHolesReported
	@OneToMany(fetch = FetchType.LAZY, mappedBy="status")
	private List<PotHolesReported> potHolesReporteds;

	//bi-directional many-to-one association to RodentBaiting
	@OneToMany(fetch = FetchType.LAZY, mappedBy="status")
	private List<RodentBaiting> rodentBaitings;

	//bi-directional many-to-one association to SanitationCodeComplaint
	@OneToMany(fetch = FetchType.LAZY, mappedBy="status")
	private List<SanitationCodeComplaint> sanitationCodeComplaints;

	//bi-directional many-to-one association to Location
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="locationid", nullable = false)
	private Location location;

	//bi-directional many-to-one association to Request
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="typeid", nullable = false)
	private Request request;

	//bi-directional many-to-one association to TreeDebri
	@OneToMany(fetch = FetchType.LAZY, mappedBy="status")
	private List<TreeDebri> treeDebris;

	//bi-directional many-to-one association to TreeTrim
	@OneToMany(fetch = FetchType.LAZY, mappedBy="status")
	private List<TreeTrim> treeTrims;

	//bi-directional many-to-one association to StreetLightsAllOut
	@OneToMany(fetch = FetchType.LAZY, mappedBy="status")
	private List<StreetLightsAllOut> streetLightsAllOuts;

	//bi-directional many-to-one association to StreetLightsOneOut
	@OneToMany(fetch = FetchType.LAZY, mappedBy="status")
	private List<StreetLightsOneOut> streetLightsOneOuts;


	public Status() {
	}

	public Long getStatusid() {
		return this.statusid;
	}

	public void setStatusid(Long statusid) {
		this.statusid = statusid;
	}

	public Date getCompletionDate() {
		return this.completionDate;
	}

	public void setCompletionDate(Date completionDate) {
		this.completionDate = completionDate;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getServiceRequestNumber() {
		return this.serviceRequestNumber;
	}

	public void setServiceRequestNumber(String serviceRequestNumber) {
		this.serviceRequestNumber = serviceRequestNumber;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<AbandonedVehicle> getAbandonedVehicles() {
		return this.abandonedVehicles;
	}

	public void setAbandonedVehicles(List<AbandonedVehicle> abandonedVehicles) {
		this.abandonedVehicles = abandonedVehicles;
	}

	public AbandonedVehicle addAbandonedVehicle(AbandonedVehicle abandonedVehicle) {
		getAbandonedVehicles().add(abandonedVehicle);
		abandonedVehicle.setStatus(this);

		return abandonedVehicle;
	}

	public AbandonedVehicle removeAbandonedVehicle(AbandonedVehicle abandonedVehicle) {
		getAbandonedVehicles().remove(abandonedVehicle);
		abandonedVehicle.setStatus(null);

		return abandonedVehicle;
	}

	public List<GarbageCart> getGarbageCarts() {
		return this.garbageCarts;
	}

	public void setGarbageCarts(List<GarbageCart> garbageCarts) {
		this.garbageCarts = garbageCarts;
	}

	public GarbageCart addGarbageCart(GarbageCart garbageCart) {
		getGarbageCarts().add(garbageCart);
		garbageCart.setStatus(this);

		return garbageCart;
	}

	public GarbageCart removeGarbageCart(GarbageCart garbageCart) {
		getGarbageCarts().remove(garbageCart);
		garbageCart.setStatus(null);

		return garbageCart;
	}

	public List<GraffitiRemoval> getGraffitiRemovals() {
		return this.graffitiRemovals;
	}

	public void setGraffitiRemovals(List<GraffitiRemoval> graffitiRemovals) {
		this.graffitiRemovals = graffitiRemovals;
	}

	public GraffitiRemoval addGraffitiRemoval(GraffitiRemoval graffitiRemoval) {
		getGraffitiRemovals().add(graffitiRemoval);
		graffitiRemoval.setStatus(this);

		return graffitiRemoval;
	}

	public GraffitiRemoval removeGraffitiRemoval(GraffitiRemoval graffitiRemoval) {
		getGraffitiRemovals().remove(graffitiRemoval);
		graffitiRemoval.setStatus(null);

		return graffitiRemoval;
	}

	public List<PotHolesReported> getPotHolesReporteds() {
		return this.potHolesReporteds;
	}

	public void setPotHolesReporteds(List<PotHolesReported> potHolesReporteds) {
		this.potHolesReporteds = potHolesReporteds;
	}

	public PotHolesReported addPotHolesReported(PotHolesReported potHolesReported) {
		getPotHolesReporteds().add(potHolesReported);
		potHolesReported.setStatus(this);

		return potHolesReported;
	}

	public PotHolesReported removePotHolesReported(PotHolesReported potHolesReported) {
		getPotHolesReporteds().remove(potHolesReported);
		potHolesReported.setStatus(null);

		return potHolesReported;
	}

	public List<RodentBaiting> getRodentBaitings() {
		return this.rodentBaitings;
	}

	public void setRodentBaitings(List<RodentBaiting> rodentBaitings) {
		this.rodentBaitings = rodentBaitings;
	}

	public RodentBaiting addRodentBaiting(RodentBaiting rodentBaiting) {
		getRodentBaitings().add(rodentBaiting);
		rodentBaiting.setStatus(this);

		return rodentBaiting;
	}

	public RodentBaiting removeRodentBaiting(RodentBaiting rodentBaiting) {
		getRodentBaitings().remove(rodentBaiting);
		rodentBaiting.setStatus(null);

		return rodentBaiting;
	}

	public List<SanitationCodeComplaint> getSanitationCodeComplaints() {
		return this.sanitationCodeComplaints;
	}

	public void setSanitationCodeComplaints(List<SanitationCodeComplaint> sanitationCodeComplaints) {
		this.sanitationCodeComplaints = sanitationCodeComplaints;
	}

	public SanitationCodeComplaint addSanitationCodeComplaint(SanitationCodeComplaint sanitationCodeComplaint) {
		getSanitationCodeComplaints().add(sanitationCodeComplaint);
		sanitationCodeComplaint.setStatus(this);

		return sanitationCodeComplaint;
	}

	public SanitationCodeComplaint removeSanitationCodeComplaint(SanitationCodeComplaint sanitationCodeComplaint) {
		getSanitationCodeComplaints().remove(sanitationCodeComplaint);
		sanitationCodeComplaint.setStatus(null);

		return sanitationCodeComplaint;
	}

	public Location getLocation() {
		return this.location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Request getRequest() {
		return this.request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	public List<TreeDebri> getTreeDebris() {
		return this.treeDebris;
	}

	public void setTreeDebris(List<TreeDebri> treeDebris) {
		this.treeDebris = treeDebris;
	}

	public TreeDebri addTreeDebri(TreeDebri treeDebri) {
		getTreeDebris().add(treeDebri);
		treeDebri.setStatus(this);

		return treeDebri;
	}

	public TreeDebri removeTreeDebri(TreeDebri treeDebri) {
		getTreeDebris().remove(treeDebri);
		treeDebri.setStatus(null);

		return treeDebri;
	}

	public List<TreeTrim> getTreeTrims() {
		return this.treeTrims;
	}

	public void setTreeTrims(List<TreeTrim> treeTrims) {
		this.treeTrims = treeTrims;
	}

	public TreeTrim addTreeTrim(TreeTrim treeTrim) {
		getTreeTrims().add(treeTrim);
		treeTrim.setStatus(this);

		return treeTrim;
	}

	public TreeTrim removeTreeTrim(TreeTrim treeTrim) {
		getTreeTrims().remove(treeTrim);
		treeTrim.setStatus(null);

		return treeTrim;
	}

}
