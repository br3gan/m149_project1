package di.postgrad.databasesystems.project1.service;


import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import di.postgrad.databasesystems.project1.domain.TreeTrim;
import di.postgrad.databasesystems.project1.repository.TreeTrimRepository;

@Service
public class TreeTrimService {

    @Autowired
    TreeTrimRepository treeTrimRepository;

    @Autowired
    LocationSpecificRequestsService locationSpecificRequestsService;

    public TreeTrim addTreeTrim(TreeTrim treeTrim) {
        treeTrimRepository.save(treeTrim);
        return treeTrim;
    }

    public Iterable<TreeTrim> getTreeTrims(){
        return treeTrimRepository.findAll();
    }

    public TreeTrim getTreeTrim(Long treeTrimId){
      return treeTrimRepository.findOne(treeTrimId);
  }
    public Iterable<TreeTrim> findLocationSpecificTreeTrims(String username, String street_address, Integer zip_code){

        Iterable<BigInteger> tree_trims_id = locationSpecificRequestsService.findLocationSpecificTreeTrims(username, street_address, zip_code);
        List<TreeTrim> tree_trim_list = new ArrayList<>();
        if (tree_trims_id != null) {
        	for (BigInteger id : tree_trims_id) {
            	tree_trim_list.add(treeTrimRepository.findOne(id.longValue()));
            }
        }
        
        return (Iterable<TreeTrim>) tree_trim_list;
    }
}
