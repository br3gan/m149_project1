package di.postgrad.databasesystems.project1.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the abandoned_vehicles database table.
 * 
 */
@Entity
@Table(name="abandoned_vehicles")
public class AbandonedVehicle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name="how_many_days_has_the_vehicle_been_reported_as_parked")
	private Float howManyDaysHasTheVehicleBeenReportedAsParked;

	@Column(name="license_plate")
	private String licensePlate;

	@Column(name="vehicle_color")
	private String vehicleColor;

	@Column(name="vehicle_make_model")
	private String vehicleMakeModel;

	//bi-directional many-to-one association to Action
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="actionsid", nullable = false)
	private Action action;

	//bi-directional many-to-one association to Status
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="statusid", nullable = false)
	private Status status;

	public AbandonedVehicle() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Float getHowManyDaysHasTheVehicleBeenReportedAsParked() {
		return this.howManyDaysHasTheVehicleBeenReportedAsParked;
	}

	public void setHowManyDaysHasTheVehicleBeenReportedAsParked(Float howManyDaysHasTheVehicleBeenReportedAsParked) {
		this.howManyDaysHasTheVehicleBeenReportedAsParked = howManyDaysHasTheVehicleBeenReportedAsParked;
	}

	public String getLicensePlate() {
		return this.licensePlate;
	}

	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}

	public String getVehicleColor() {
		return this.vehicleColor;
	}

	public void setVehicleColor(String vehicleColor) {
		this.vehicleColor = vehicleColor;
	}

	public String getVehicleMakeModel() {
		return this.vehicleMakeModel;
	}

	public void setVehicleMakeModel(String vehicleMakeModel) {
		this.vehicleMakeModel = vehicleMakeModel;
	}

	public Action getAction() {
		return this.action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	public Status getStatus() {
		return this.status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

}