package di.postgrad.databasesystems.project1.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the rodent_baiting database table.
 * 
 */
@Entity
@Table(name="rodent_baiting")
public class RodentBaiting implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name="number_of_premises_baited")
	private Float numberOfPremisesBaited;

	@Column(name="number_of_premises_with_garbage")
	private Float numberOfPremisesWithGarbage;

	@Column(name="number_of_premises_with_rats")
	private Float numberOfPremisesWithRats;

	//bi-directional many-to-one association to Action
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="actionsid", nullable = false)
	private Action action;

	//bi-directional many-to-one association to Status
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="statusid", nullable = false)
	private Status status;

	public RodentBaiting() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Float getNumberOfPremisesBaited() {
		return this.numberOfPremisesBaited;
	}

	public void setNumberOfPremisesBaited(Float numberOfPremisesBaited) {
		this.numberOfPremisesBaited = numberOfPremisesBaited;
	}

	public Float getNumberOfPremisesWithGarbage() {
		return this.numberOfPremisesWithGarbage;
	}

	public void setNumberOfPremisesWithGarbage(Float numberOfPremisesWithGarbage) {
		this.numberOfPremisesWithGarbage = numberOfPremisesWithGarbage;
	}

	public Float getNumberOfPremisesWithRats() {
		return this.numberOfPremisesWithRats;
	}

	public void setNumberOfPremisesWithRats(Float numberOfPremisesWithRats) {
		this.numberOfPremisesWithRats = numberOfPremisesWithRats;
	}

	public Action getAction() {
		return this.action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	public Status getStatus() {
		return this.status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

}