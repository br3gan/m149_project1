package di.postgrad.databasesystems.project1.service;


import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import di.postgrad.databasesystems.project1.domain.GarbageCart;
import di.postgrad.databasesystems.project1.repository.GarbageCartRepository;

@Service
public class GarbageCartService {

    @Autowired
    GarbageCartRepository garbageCartRepository;

    @Autowired
    LocationSpecificRequestsService locationSpecificRequestsService;

    public GarbageCart addGarbageCart(GarbageCart garbageCart) {
        garbageCartRepository.save(garbageCart);
        return garbageCart;
    }

    public Iterable<GarbageCart> getGarbageCarts(){
        return garbageCartRepository.findAll();
    }

    public GarbageCart getGarbageCart(Long garbageCartId){
      return garbageCartRepository.findOne(garbageCartId);
  }

    public Iterable<GarbageCart> findLocationSpecificGarbageCarts(String username, String street_address, Integer zip_code){

        Iterable<BigInteger> garbage_carts_id = locationSpecificRequestsService.findLocationSpecificGarbageCarts(username, street_address, zip_code);
        List<GarbageCart> garbage_cart_list = new ArrayList<>();
        if (garbage_carts_id != null) {
        	for (BigInteger id : garbage_carts_id) {
            	garbage_cart_list.add(garbageCartRepository.findOne(id.longValue()));
            }
        }
        
        return (Iterable<GarbageCart>) garbage_cart_list;
    }
}
