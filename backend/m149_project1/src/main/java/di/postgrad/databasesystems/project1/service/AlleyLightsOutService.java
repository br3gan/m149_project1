package di.postgrad.databasesystems.project1.service;


import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import di.postgrad.databasesystems.project1.domain.AlleyLightsOut;
import di.postgrad.databasesystems.project1.repository.AlleyLightsOutRepository;

@Service
public class AlleyLightsOutService {

    @Autowired
    AlleyLightsOutRepository alleyLightsOutRepository;
    
    @Autowired
    LocationSpecificRequestsService locationSpecificRequestsService;

    public AlleyLightsOut addAlleyLightsOut(AlleyLightsOut alleyLightsOut) {
        alleyLightsOutRepository.save(alleyLightsOut);
        return alleyLightsOut;
    }

    public Iterable<AlleyLightsOut> getAlleyLightsOuts(){
        return alleyLightsOutRepository.findAll();
    }

    public AlleyLightsOut getAlleyLightsOut(Long alleyLightsOutId){
      return alleyLightsOutRepository.findOne(alleyLightsOutId);
  }
    
    public Iterable<AlleyLightsOut> findLocationSpecificAlleyLightsOut(String username, String street_address, Integer zip_code){
    	
        Iterable<BigInteger> alley_lights_outs_id = locationSpecificRequestsService.findLocationSpecificAlleyLightsOut(username, street_address, zip_code);
        List<AlleyLightsOut> alley_lights_out_list = new ArrayList<>();
        if (alley_lights_outs_id != null) {
        	for (BigInteger id : alley_lights_outs_id) {
            	alley_lights_out_list.add(alleyLightsOutRepository.findOne(id.longValue()));
            }
        }
        
        return (Iterable<AlleyLightsOut>) alley_lights_out_list;
    }
}