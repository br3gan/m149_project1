-- Create indices
DROP INDEX location_id_index; CREATE INDEX location_id_index ON location(locationid);
DROP INDEX status_id_index; CREATE INDEX status_id_index ON status(statusid);
DROP INDEX actions_id_index; CREATE INDEX actions_id_index ON actions(actionsid);

DROP INDEX abandoned_vehicles_id_index; CREATE INDEX abandoned_vehicles_id_index ON abandoned_vehicles(id);
DROP INDEX allay_lights_out_id_index; CREATE INDEX allay_lights_out_id_index ON allay_lights_out(id);
DROP INDEX garbage_carts_id_index; CREATE INDEX garbage_carts_id_index ON garbage_carts(id);
DROP INDEX graffiti_removal_id_index; CREATE INDEX graffiti_removal_id_index ON graffiti_removal(id);
DROP INDEX pot_holes_reported_id_index; CREATE INDEX pot_holes_reported_id_index ON pot_holes_reported(id);
DROP INDEX requests_id_index; CREATE INDEX requests_id_index ON requests(typeid);
DROP INDEX rodent_baiting_id_index; CREATE INDEX rodent_baiting_id_index ON rodent_baiting(id);
DROP INDEX sanitation_code_complaints_id_index; CREATE INDEX sanitation_code_complaints_id_index ON sanitation_code_complaints(id);
DROP INDEX street_lights_all_out_id_index; CREATE INDEX street_lights_all_out_id_index ON street_lights_all_out(id);
DROP INDEX street_lights_one_out_id_index; CREATE INDEX street_lights_one_out_id_index ON street_lights_one_out(id);
DROP INDEX tree_debris_id_index; CREATE INDEX tree_debris_id_index ON tree_debris(id);
DROP INDEX tree_trims_id_index; CREATE INDEX tree_trims_id_index ON tree_trims(id);

DROP INDEX app_user_id_index; CREATE INDEX app_user_id_index ON app_user(username);
DROP INDEX user_queries_id_index; CREATE INDEX user_queries_id_index ON user_queries(id);
