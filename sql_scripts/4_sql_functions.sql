DROP FUNCTION function_0X;
CREATE OR REPLACE FUNCTION function_0X ()
RETURNS TABLE (

)
AS $$
BEGIN
    RETURN QUERY

END; $$
LANGUAGE 'plpgsql';

DROP FUNCTION function_01;
CREATE OR REPLACE FUNCTION function_01 (_starting_date DATE, _ending_date DATE)
RETURNS TABLE (
    type_of_service_request TEXT,
    counter BIGINT
)
AS $$
BEGIN
    RETURN QUERY
    SELECT r.type_of_service_request, COUNT(r.typeid)
    FROM requests AS r, status AS s
    WHERE r.typeid=s.typeid AND daterange(_starting_date, _ending_date, '[]') @> s.creation_date
    GROUP BY(r.typeid)
    ORDER BY (COUNT) DESC;
END; $$
LANGUAGE 'plpgsql';

DROP FUNCTION function_02;
CREATE OR REPLACE FUNCTION function_02(_type_of_service_request TEXT, _starting_date DATE, _ending_date DATE)
RETURNS TABLE (
    creationDate date,
    counter bigint
)
AS $$
BEGIN
    RETURN QUERY
    SELECT creation_date, COUNT(*)
    FROM
    (
        SELECT s.creation_date
        FROM status AS s, requests AS r
        WHERE (
            (r.type_of_service_request = _type_of_service_request)
            AND (s.typeId = r.typeId)
            AND (daterange(_starting_date, _ending_date, '[]') @> s.creation_date)
        )
    ) as q
    GROUP BY (creation_date)
    ORDER BY (creation_date);
END; $$
LANGUAGE 'plpgsql';

DROP FUNCTION function_03;
CREATE OR REPLACE FUNCTION function_03(_date DATE)
RETURNS TABLE (
    type_of_service TEXT,
    zip_code INTEGER,
    counter BIGINT
)
AS $$
BEGIN
    RETURN QUERY
    WITH my_day AS(
            SELECT s.typeID, s.locationID
            FROM status AS s
            WHERE s.creation_date=_date
            ),
    my_zip_code_group AS(
        SELECT d.typeId, l.zip_code, count(*)
        FROM my_day AS d, location AS l
        WHERE (
            l.locationId=d.locationId
        )
        GROUP BY l.zip_code, d.typeID
    ),
    my_max AS(
        select mz.zip_code, MAX(count)
        FROM my_zip_code_group AS mz
        GROUP BY mz.zip_code
    )
    SELECT r.type_of_service_request, g.zip_code, g.count
    FROM my_zip_code_group as g, requests AS r
    WHERE g.typeId=r.typeid
        AND (
            SELECT m.max
            FROM my_max as m
            WHERE m.zip_code = g.zip_code
        ) = g.count;
END; $$
LANGUAGE 'plpgsql';

DROP FUNCTION function_04;
CREATE OR REPLACE FUNCTION function_04 (_starting_date DATE, _ending_date DATE)
RETURNS TABLE (
    type_of_service_request TEXT,
    average BIGINT
)
AS $$
BEGIN
    RETURN QUERY
    SELECT r.type_of_service_request, SUM(s.completion_date-s.creation_date)/COUNT(r.type_of_service_request) AS avg
    FROM requests AS r, status AS s
    WHERE r.typeid=s.typeid AND daterange(_starting_date, _ending_date, '[]') @> s.creation_date
    GROUP BY(r.type_of_service_request);
END; $$
LANGUAGE 'plpgsql';


DROP FUNCTION function_05;
CREATE OR REPLACE FUNCTION function_05 (_min_lon FLOAT, _max_lon FLOAT, _min_lat FLOAT, _max_lat FLOAT, _creation_date DATE)
RETURNS TABLE (
    type_of_service_request TEXT,
    count BIGINT
)
AS $$
BEGIN
    RETURN QUERY
    SELECT r.type_of_service_request, COUNT(r.type_of_service_request)
    FROM requests AS r, status AS s, location AS l
    WHERE r.typeid=s.typeid
    AND s.locationid=l.locationid
    AND s.creation_date=_creation_date
    AND (
        (l.latitude> _min_lat AND l.latitude<_max_lat)
        AND (l.longitude>_min_lon AND l.longitude<_max_lon)
    )
    GROUP BY r.type_of_service_request
    ORDER BY count DESC
    LIMIT 1;
END; $$
LANGUAGE 'plpgsql';

-- 6
DROP FUNCTION function_06;
CREATE OR REPLACE FUNCTION function_06 (_starting_date DATE, _ending_date DATE)
RETURNS TABLE (
    ssa SMALLINT,
    count BIGINT
)
AS $$
BEGIN
    RETURN QUERY
    WITH requests_per_ssa AS (
        SELECT l.ssa, COUNT(r.type_of_service_request)
        FROM location AS l, status AS s, requests AS r
        WHERE (l.ssa IS NOT NULL)
        AND (l.locationid=s.locationid) AND (s.typeid=r.typeid)
        AND (daterange(_starting_date, _ending_date, '[]') @> s.creation_date)
        GROUP BY l.ssa
    ),
    total_days as (
        select _ending_date::date - _starting_date::date as days
    )
    SELECT r.ssa, r.count/d.days
    FROM requests_per_ssa AS r, total_days as d
    ORDER BY r.count DESC
    LIMIT 5;
END; $$
LANGUAGE 'plpgsql';
--


-- 6b
DROP FUNCTION function_06b;
CREATE OR REPLACE FUNCTION function_06b (_starting_date DATE, _ending_date DATE)
RETURNS TABLE (
    ssa SMALLINT,
    creation_date DATE,
    count BIGINT
)
AS $$
BEGIN
    RETURN QUERY
    WITH requests_per_ssa_per_date AS (
        SELECT l.ssa, s.creation_date, COUNT(r.type_of_service_request)
        FROM location AS l, status AS s, requests AS r
        WHERE (l.ssa IS NOT NULL)
        AND (l.locationid=s.locationid) AND (s.typeid=r.typeid)
        AND (daterange(_starting_date, _ending_date, '[]') @> s.creation_date)
        GROUP BY l.ssa, s.creation_date
    ),
    my_max as(
        SELECT r.creation_date, MAX(r.count)
        FROM requests_per_ssa_per_date AS r
        GROUP BY r.creation_date
    )
    SELECT r.ssa, r.creation_date, r.count
    FROM requests_per_ssa_per_date AS r
    WHERE (
        SELECT MAX
        FROM my_max AS m
        WHERE m.creation_date=r.creation_date
    )=r.count
    ORDER BY r.count DESC
    LIMIT 5;
END; $$
LANGUAGE 'plpgsql';
--

-- license plate has stupid values
-- 7
DROP FUNCTION function_07;
CREATE OR REPLACE FUNCTION function_07 ()
RETURNS TABLE (
    license_plate TEXT,
    count BIGINT
)
AS $$
BEGIN
    RETURN QUERY
    SELECT a.license_plate, COUNT(a.license_plate)
    FROM abandoned_vehicles AS a
    WHERE a.license_plate IS NOT NULL
    GROUP BY a.license_plate
    HAVING(COUNT(a.license_plate)>1)
    ORDER BY COUNT DESC;
END; $$
LANGUAGE 'plpgsql';
--

-- 8
DROP FUNCTION function_08;
CREATE OR REPLACE FUNCTION function_08 ()
RETURNS TABLE (
    color VARCHAR(150),
    count BIGINT
)
AS $$
BEGIN
    RETURN QUERY
    SELECT *
    FROM
    (
        SELECT a.vehicle_color, COUNT(a.vehicle_color)
        FROM abandoned_vehicles AS a
        WHERE a.vehicle_color IS NOT NULL
        GROUP BY a.vehicle_color
        HAVING(COUNT(a.vehicle_color)>1)
        ORDER BY COUNT DESC
        LIMIT 2
    ) AS t
    ORDER BY COUNT DESC LIMIT 1;
END; $$
LANGUAGE 'plpgsql';
--

-- WIP
-- 9
DROP FUNCTION function_09;
CREATE OR REPLACE FUNCTION function_09 (_number_of_premises_baited REAL)
RETURNS TABLE (
    creation_date DATE,
    status VARCHAR(150),
    completion_date DATE,
    service_request_number VARCHAR(150),
    type_of_service_request TEXT,
    number_of_premises_baited REAL,
    number_of_premises_with_garbage REAL,
    number_of_premises_with_rats REAL,
    current_activity TEXT,
    most_recent_action TEXT,
    street_address VARCHAR(150),
    zip_code INTEGER,
    x_coordinate DOUBLE PRECISION,
    y_coordinate DOUBLE PRECISION,
    ward SMALLINT,
    police_district SMALLINT,
    community_area SMALLINT,
    latitude double PRECISION,
    longitude double PRECISION,
    location TEXT
)
AS $$
BEGIN
    RETURN QUERY
    SELECT s.creation_date, s.status, s.completion_date, s.service_request_number, r.type_of_service_request, rb.number_of_premises_baited, rb.number_of_premises_with_garbage, rb.number_of_premises_with_rats, a.current_activity, a.most_recent_action, l.street_address, l.zip_code, l.x_coordinate, l.y_coordinate, l.ward, l.police_district, l.community_area, l.latitude, l.longitude, l.location
    FROM requests AS r, status AS s, location AS l, actions AS a, rodent_baiting as rb
    WHERE (r.typeId=s.typeid)
        AND (s.statusid=rb.statusid)
        AND (s.locationid=l.locationid)
        AND (a.actionsid=rb.actionsid)
        AND (rb.number_of_premises_baited IS NOT NULL)
        AND (rb.number_of_premises_baited<_number_of_premises_baited);
END; $$
LANGUAGE 'plpgsql';
--

-- 10
DROP FUNCTION function_10;
CREATE OR REPLACE FUNCTION function_10 (_max_number_of_premises_with_garbage REAL)
RETURNS TABLE (
    creation_date DATE,
    status VARCHAR(150),
    completion_date DATE,
    service_request_number VARCHAR(150),
    type_of_service_request TEXT,
    number_of_premises_baited REAL,
    number_of_premises_with_garbage REAL,
    number_of_premises_with_rats REAL,
    current_activity TEXT,
    most_recent_action TEXT,
    street_address VARCHAR(150),
    zip_code INTEGER,
    x_coordinate DOUBLE PRECISION,
    y_coordinate DOUBLE PRECISION,
    ward SMALLINT,
    police_district SMALLINT,
    community_area SMALLINT,
    latitude double PRECISION,
    longitude double PRECISION,
    location TEXT
)
AS $$
BEGIN
    RETURN QUERY
    SELECT s.creation_date, s.status, s.completion_date, s.service_request_number, r.type_of_service_request, rb.number_of_premises_baited, rb.number_of_premises_with_garbage, rb.number_of_premises_with_rats, a.current_activity, a.most_recent_action, l.street_address, l.zip_code, l.x_coordinate, l.y_coordinate, l.ward, l.police_district, l.community_area, l.latitude, l.longitude, l.location
    FROM requests AS r, status AS s, location AS l, actions AS a, rodent_baiting as rb
    WHERE (r.typeId=s.typeid)
        AND (s.statusid=rb.statusid)
        AND (s.locationid=l.locationid)
        AND (a.actionsid=rb.actionsid)
        AND (rb.number_of_premises_with_garbage IS NOT NULL)
        AND (rb.number_of_premises_baited<_max_number_of_premises_with_garbage);
END; $$
LANGUAGE 'plpgsql';
--

-- 11
DROP FUNCTION function_11;
CREATE OR REPLACE FUNCTION function_11 (_max_number_of_premises_with_rats REAL)
RETURNS TABLE (
    creation_date DATE,
    status VARCHAR(150),
    completion_date DATE,
    service_request_number VARCHAR(150),
    type_of_service_request TEXT,
    number_of_premises_baited REAL,
    number_of_premises_with_garbage REAL,
    number_of_premises_with_rats REAL,
    current_activity TEXT,
    most_recent_action TEXT,
    street_address VARCHAR(150),
    zip_code INTEGER,
    x_coordinate DOUBLE PRECISION,
    y_coordinate DOUBLE PRECISION,
    ward SMALLINT,
    police_district SMALLINT,
    community_area SMALLINT,
    latitude double PRECISION,
    longitude double PRECISION,
    location TEXT
)
AS $$
BEGIN
    RETURN QUERY
    SELECT s.creation_date, s.status, s.completion_date, s.service_request_number, r.type_of_service_request, rb.number_of_premises_baited, rb.number_of_premises_with_garbage, rb.number_of_premises_with_rats, a.current_activity, a.most_recent_action, l.street_address, l.zip_code, l.x_coordinate, l.y_coordinate, l.ward, l.police_district, l.community_area, l.latitude, l.longitude, l.location
    FROM requests AS r, status AS s, location AS l, actions AS a, rodent_baiting as rb
    WHERE (r.typeId=s.typeid)
        AND (s.statusid=rb.statusid)
        AND (s.locationid=l.locationid)
        AND (a.actionsid=rb.actionsid)
        AND (rb.number_of_premises_with_rats IS NOT NULL)
        AND (rb.number_of_premises_baited<_max_number_of_premises_with_rats);
END; $$
LANGUAGE 'plpgsql';
--

-- 12
DROP FUNCTION function_12;
CREATE OR REPLACE FUNCTION function_12 (_number_of_potholes_filled_on_block REAL, _number_of_premises_baited REAL, _completion_date DATE)
RETURNS TABLE (
police_district SMALLINT
)
AS $$
BEGIN
    RETURN QUERY
        WITH valid_police_districts AS (
            SELECT l.police_district
            FROM status AS s, location AS l, pot_holes_reported AS ph
        WHERE
            (s.statusid=ph.statusid)
            AND (s.locationid=l.locationid )
            AND (s.creation_date=s.completion_date)
            AND (ph.number_of_potholes_filled_on_block>_number_of_potholes_filled_on_block)
        )
        SELECT DISTINCT l.police_district
        FROM status AS s, location AS l, rodent_baiting AS rb, valid_police_districts AS vpd
        WHERE s.locationid=l.locationid
            AND s.statusid=rb.statusid
            AND s.completion_date=_completion_date
            AND rb.number_of_premises_baited>_number_of_premises_baited
            AND l.police_district=vpd.police_district;
    END; $$
    LANGUAGE 'plpgsql';

SELECT current_time;
