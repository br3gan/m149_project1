-- create table
drop table app_user cascade;
create table app_user
(
    username            varchar(36) not null,
    encrypted_password  varchar(128) not null,
    address             varchar(128) not null,
    email               varchar(64) not null,
    role                varchar(30) not null,
    enabled             boolean not null
);
--
alter table app_user
add constraint app_user_pk primary key (username);

drop table user_queries cascade;
create table user_queries
(
    id                  bigserial not null,
    username            varchar(36) not null,
    query               text not null,
    time                timestamp not null
);
--
alter table user_queries
    add constraint user_queries_pk primary key (id);
alter table user_queries
    add constraint user_queries_fk foreign key (username)
    references app_user(username);

-- used by spring remember me api.
drop table persistent_logins cascade;
create table persistent_logins (
    username            varchar(64) not null,
    series              varchar(64) not null,
    token               varchar(64) not null,
    last_used           timestamp not null,
    primary key (series)
);

insert into app_user (username, encrypted_password, address, email, role, enabled)
values ('admin', '$2a$10$lpBMzRn0aZOkSkpZPePffOX07G5wz41biArYWOegl8Cc3y7VANVAq', '1st Street', 'mail@email.com', 'ADMIN' , true);

--------------------------------------
