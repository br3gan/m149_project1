DROP FUNCTION handle_insert_to_abandoned_vehicles_tmp CASCADE;


SELECT current_time;

CREATE OR REPLACE FUNCTION handle_insert_to_abandoned_vehicles_tmp() RETURNS TRIGGER AS
$BODY$
DECLARE
    _typeID int=
    (
        SELECT typeID
        FROM requests AS r
        WHERE
            r.type_of_service_request=new.type_of_service_request OR (r.type_of_service_request IS NULL AND new.type_of_service_request IS NULL)
        LIMIT 1
    );

    _locationID int=
    (
        SELECT locationID
        FROM location AS l
        WHERE
            l.location=new.location OR (l.location IS NULL AND new.location IS NULL)
            AND (l.longitude=new.longitude OR (l.longitude IS NULL AND new.longitude IS NULL))
            AND (l.latitude=new.latitude OR (l.latitude IS NULL AND new.latitude IS NULL))
            AND (l.ssa=new.ssa OR (l.ssa IS NULL AND new.ssa IS NULL))
            AND (l.x_coordinate=new.x_coordinate OR (l.x_coordinate IS NULL AND new.x_coordinate IS NULL))
            AND (l.y_coordinate=new.y_coordinate OR (l.y_coordinate IS NULL AND new.y_coordinate IS NULL))
            AND (l.street_address = new.street_address OR (l.street_address  IS NULL AND  new.street_address IS NULL))
            AND (l.zip_code=new.zip_code OR (l.zip_code IS NULL AND new.zip_code IS NULL))
            AND (l.community_area=new.community_area OR (l.community_area IS NULL AND new.community_area IS NULL))
            AND (l.ward=new.ward OR (l.ward IS NULL AND new.ward IS NULL))
            AND (l.police_district=new.police_district OR (l.police_district IS NULL AND new.police_district IS NULL))
        LIMIT 1
    );

    _statusID int=NULL;

    _actionsID int=
    (
        SELECT actionsID
        FROM actions AS a
        WHERE
            a.current_activity=new.current_activity
            AND a.most_recent_action=new.most_recent_action
        LIMIT 1
    );

BEGIN

    IF _typeID IS NOT NULL AND _locationID IS NOT NULL
    THEN
        _statusID=
        (
            SELECT statusID
            FROM status as r
            WHERE
                    (r.typeID=_typeID)
                    AND (r.locationID=_locationID)
                    AND ((r.creation_date=new.creation_date) OR (r.creation_date is null AND new.creation_date is null))
                    AND ((r.status=new.status) OR (r.status is null AND new.status is null))
                    AND ((r.completion_date=new.completion_date) OR (r.completion_date is null AND new.completion_date is null))
                    AND ((r.service_request_number=new.service_request_number) OR (r.service_request_number is null AND new.service_request_number is null))
            LIMIT 1
        );
    END IF;

    IF _typeID IS NULL
    THEN
        WITH req AS
        (
            INSERT INTO requests(type_of_service_request)
            VALUES(new.type_of_service_request)
            RETURNING typeID
        )
        SELECT typeID INTO _typeID FROM req;
    END IF;

    IF _locationID IS NULL
    THEN
        WITH loc AS
        (
            INSERT INTO location(street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, ssa, latitude, longitude, location)
            VALUES(new.street_address, new.zip_code, new.x_coordinate, new.y_coordinate, new.ward, new.police_district, new.community_area, new.ssa, new.latitude, new.longitude, new.location)
            RETURNING locationID
        )
        SELECT locationID INTO _locationID FROM loc;
    END IF;

    IF _statusID IS NULL
    THEN
        WITH stat AS
        (
            INSERT INTO status(typeID, service_request_number, creation_date, status, completion_date, locationID)
            VALUES (_typeID, new.service_request_number, new.creation_date, new.status, new.completion_date, _locationID)
            RETURNING statusID
        )
        SELECT statusID INTO _statusID FROM stat;
    END IF;

    IF _actionsID IS NULL
    THEN
        WITH act AS
        (
            INSERT INTO actions(current_activity, most_recent_action)
            VALUES (new.current_activity, new.most_recent_action)
            RETURNING actionsID
        )
        SELECT actionsID INTO _actionsID FROM act;
    END IF;

    INSERT INTO abandoned_vehicles(statusID, actionsID, license_plate, vehicle_make_model, vehicle_color, how_many_days_has_the_vehicle_been_reported_as_parked)
    SELECT _statusID, _actionsID, new.license_plate, new.vehicle_make_model, new.vehicle_color, new.how_many_days_has_the_vehicle_been_reported_as_parked;
    RETURN new;
END;
$BODY$
language plpgsql;

CREATE TRIGGER abandoned_vehicles_trigger
     AFTER INSERT ON abandoned_vehicles_tmp
     FOR EACH ROW
     EXECUTE PROCEDURE handle_insert_to_abandoned_vehicles_tmp();
DROP FUNCTION handle_insert_to_alley_lights_out_tmp CASCADE;

SELECT current_time;

CREATE OR REPLACE FUNCTION handle_insert_to_alley_lights_out_tmp() RETURNS TRIGGER AS
$BODY$
DECLARE
    _typeID int=
    (
        SELECT typeID
        FROM requests AS r
        WHERE
            r.type_of_service_request=new.type_of_service_request OR (r.type_of_service_request IS NULL AND new.type_of_service_request IS NULL)
        LIMIT 1
    );

    _locationID int=
    (
        SELECT locationID
        FROM location AS l
        WHERE
            l.location=new.location OR (l.location IS NULL AND new.location IS NULL)
            AND (l.longitude=new.longitude OR (l.longitude IS NULL AND new.longitude IS NULL))
            AND (l.latitude=new.latitude OR (l.latitude IS NULL AND new.latitude IS NULL))
            AND (l.x_coordinate=new.x_coordinate OR (l.x_coordinate IS NULL AND new.x_coordinate IS NULL))
            AND (l.y_coordinate=new.y_coordinate OR (l.y_coordinate IS NULL AND new.y_coordinate IS NULL))
            AND (l.street_address = new.street_address OR (l.street_address  IS NULL AND  new.street_address IS NULL))
            AND (l.zip_code=new.zip_code OR (l.zip_code IS NULL AND new.zip_code IS NULL))
            AND (l.community_area=new.community_area OR (l.community_area IS NULL AND new.community_area IS NULL))
            AND (l.ward=new.ward OR (l.ward IS NULL AND new.ward IS NULL))
            AND (l.police_district=new.police_district OR (l.police_district IS NULL AND new.police_district IS NULL))
        LIMIT 1
    );

    _statusID int=NULL;

BEGIN

    IF _typeID IS NOT NULL AND _locationID IS NOT NULL
    THEN
        _statusID=
        (
            SELECT statusID
            FROM status as r
            WHERE
                    (r.typeID=_typeID)
                    AND (r.locationID=_locationID)
                    AND ((r.creation_date=new.creation_date) OR (r.creation_date is null AND new.creation_date is null))
                    AND ((r.status=new.status) OR (r.status is null AND new.status is null))
                    AND ((r.completion_date=new.completion_date) OR (r.completion_date is null AND new.completion_date is null))
                    AND ((r.service_request_number=new.service_request_number) OR (r.service_request_number is null AND new.service_request_number is null))
            LIMIT 1
        );
    END IF;

    IF _typeID IS NULL
    THEN
        WITH req AS
        (
            INSERT INTO requests(type_of_service_request)
            VALUES(new.type_of_service_request)
            RETURNING typeID
        )
        SELECT typeID INTO _typeID FROM req;
    END IF;

    IF _locationID IS NULL
    THEN
        WITH loc AS
        (
            INSERT INTO location(street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, latitude, longitude, location)
            VALUES(new.street_address, new.zip_code, new.x_coordinate, new.y_coordinate, new.ward, new.police_district, new.community_area, new.latitude, new.longitude, new.location)
            RETURNING locationID
        )
        SELECT locationID INTO _locationID FROM loc;
    END IF;

    IF _statusID IS NULL
    THEN
        WITH stat AS
        (
            INSERT INTO status(typeID, service_request_number, creation_date, status, completion_date, locationID)
            VALUES (_typeID, new.service_request_number, new.creation_date, new.status, new.completion_date, _locationID)
            RETURNING statusID
        )
        SELECT statusID INTO _statusID FROM stat;
    END IF;

    INSERT INTO alley_lights_out(statusID)
    SELECT _statusID
    RETURN new;
END;
$BODY$
language plpgsql;

CREATE TRIGGER alley_lights_out_trigger
     AFTER INSERT ON alley_lights_out_tmp
     FOR EACH ROW
     EXECUTE PROCEDURE handle_insert_to_alley_lights_out_tmp();
DROP FUNCTION handle_insert_to_garbage_carts_tmp CASCADE;


SELECT current_time;

CREATE OR REPLACE FUNCTION handle_insert_to_garbage_carts_tmp() RETURNS TRIGGER AS
$BODY$
DECLARE
    _typeID int=
    (
        SELECT typeID
        FROM requests AS r
        WHERE
            r.type_of_service_request=new.type_of_service_request OR (r.type_of_service_request IS NULL AND new.type_of_service_request IS NULL)
        LIMIT 1
    );

    _locationID int=
    (
        SELECT locationID
        FROM location AS l
        WHERE
            l.location=new.location OR (l.location IS NULL AND new.location IS NULL)
            AND (l.longitude=new.longitude OR (l.longitude IS NULL AND new.longitude IS NULL))
            AND (l.latitude=new.latitude OR (l.latitude IS NULL AND new.latitude IS NULL))
            AND (l.ssa=new.ssa OR (l.ssa IS NULL AND new.ssa IS NULL))
            AND (l.x_coordinate=new.x_coordinate OR (l.x_coordinate IS NULL AND new.x_coordinate IS NULL))
            AND (l.y_coordinate=new.y_coordinate OR (l.y_coordinate IS NULL AND new.y_coordinate IS NULL))
            AND (l.street_address = new.street_address OR (l.street_address  IS NULL AND  new.street_address IS NULL))
            AND (l.zip_code=new.zip_code OR (l.zip_code IS NULL AND new.zip_code IS NULL))
            AND (l.community_area=new.community_area OR (l.community_area IS NULL AND new.community_area IS NULL))
            AND (l.ward=new.ward OR (l.ward IS NULL AND new.ward IS NULL))
            AND (l.police_district=new.police_district OR (l.police_district IS NULL AND new.police_district IS NULL))
        LIMIT 1
    );

    _statusID int=NULL;

    _actionsID int=
    (
        SELECT actionsID
        FROM actions AS a
        WHERE
            a.current_activity=new.current_activity
            AND a.most_recent_action=new.most_recent_action
        LIMIT 1
    );

BEGIN

    IF _typeID IS NOT NULL AND _locationID IS NOT NULL
    THEN
        _statusID=
        (
            SELECT statusID
            FROM status as r
            WHERE
                    (r.typeID=_typeID)
                    AND (r.locationID=_locationID)
                    AND ((r.creation_date=new.creation_date) OR (r.creation_date is null AND new.creation_date is null))
                    AND ((r.status=new.status) OR (r.status is null AND new.status is null))
                    AND ((r.completion_date=new.completion_date) OR (r.completion_date is null AND new.completion_date is null))
                    AND ((r.service_request_number=new.service_request_number) OR (r.service_request_number is null AND new.service_request_number is null))
            LIMIT 1
        );
    END IF;

    IF _typeID IS NULL
    THEN
        WITH req AS
        (
            INSERT INTO requests(type_of_service_request)
            VALUES(new.type_of_service_request)
            RETURNING typeID
        )
        SELECT typeID INTO _typeID FROM req;
    END IF;

    IF _locationID IS NULL
    THEN
        WITH loc AS
        (
            INSERT INTO location(street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, ssa, latitude, longitude, location)
            VALUES(new.street_address, new.zip_code, new.x_coordinate, new.y_coordinate, new.ward, new.police_district, new.community_area, new.ssa, new.latitude, new.longitude, new.location)
            RETURNING locationID
        )
        SELECT locationID INTO _locationID FROM loc;
    END IF;

    IF _statusID IS NULL
    THEN
        WITH stat AS
        (
            INSERT INTO status(typeID, service_request_number, creation_date, status, completion_date, locationID)
            VALUES (_typeID, new.service_request_number, new.creation_date, new.status, new.completion_date, _locationID)
            RETURNING statusID
        )
        SELECT statusID INTO _statusID FROM stat;
    END IF;

    IF _actionsID IS NULL
    THEN
        WITH act AS
        (
            INSERT INTO actions(current_activity, most_recent_action)
            VALUES (new.current_activity, new.most_recent_action)
            RETURNING actionsID
        )
        SELECT actionsID INTO _actionsID FROM act;
    END IF;

    INSERT INTO garbage_carts(statusID, actionsID, number_of_black_carts_delivered)
    SELECT _statusID, _actionsID, new.number_of_black_carts_delivered;
    RETURN new;
END;
$BODY$
language plpgsql;

CREATE TRIGGER garbage_carts_trigger
     AFTER INSERT ON garbage_carts_tmp
     FOR EACH ROW
     EXECUTE PROCEDURE handle_insert_to_garbage_carts_tmp();
DROP FUNCTION handle_insert_to_graffiti_removal_tmp CASCADE;


SELECT current_time;

CREATE OR REPLACE FUNCTION handle_insert_to_graffiti_removal_tmp() RETURNS TRIGGER AS
$BODY$
DECLARE
    _typeID int=
    (
        SELECT typeID
        FROM requests AS r
        WHERE
            r.type_of_service_request=new.type_of_service_request OR (r.type_of_service_request IS NULL AND new.type_of_service_request IS NULL)
        LIMIT 1
    );

    _locationID int=
    (
        SELECT locationID
        FROM location AS l
        WHERE
            l.location=new.location OR (l.location IS NULL AND new.location IS NULL)
            AND (l.ssa=new.ssa OR (l.ssa IS NULL AND new.ssa IS NULL))
            AND (l.longitude=new.longitude OR (l.longitude IS NULL AND new.longitude IS NULL))
            AND (l.latitude=new.latitude OR (l.latitude IS NULL AND new.latitude IS NULL))
            AND (l.x_coordinate=new.x_coordinate OR (l.x_coordinate IS NULL AND new.x_coordinate IS NULL))
            AND (l.y_coordinate=new.y_coordinate OR (l.y_coordinate IS NULL AND new.y_coordinate IS NULL))
            AND (l.street_address = new.street_address OR (l.street_address  IS NULL AND  new.street_address IS NULL))
            AND (l.zip_code=new.zip_code OR (l.zip_code IS NULL AND new.zip_code IS NULL))
            AND (l.community_area=new.community_area OR (l.community_area IS NULL AND new.community_area IS NULL))
            AND (l.ward=new.ward OR (l.ward IS NULL AND new.ward IS NULL))
            AND (l.police_district=new.police_district OR (l.police_district IS NULL AND new.police_district IS NULL))
        LIMIT 1
    );

    _statusID int=NULL;

BEGIN

    IF _typeID IS NOT NULL AND _locationID IS NOT NULL
    THEN
        _statusID=
        (
            SELECT statusID
            FROM status as r
            WHERE
                    (r.typeID=_typeID)
                    AND (r.locationID=_locationID)
                    AND ((r.creation_date=new.creation_date) OR (r.creation_date is null AND new.creation_date is null))
                    AND ((r.status=new.status) OR (r.status is null AND new.status is null))
                    AND ((r.completion_date=new.completion_date) OR (r.completion_date is null AND new.completion_date is null))
                    AND ((r.service_request_number=new.service_request_number) OR (r.service_request_number is null AND new.service_request_number is null))
            LIMIT 1
        );
    END IF;

    IF _typeID IS NULL
    THEN
        WITH req AS
        (
            INSERT INTO requests(type_of_service_request)
            VALUES(new.type_of_service_request)
            RETURNING typeID
        )
        SELECT typeID INTO _typeID FROM req;
    END IF;

    IF _locationID IS NULL
    THEN
        WITH loc AS
        (
            INSERT INTO location(street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, ssa, latitude, longitude, location)
            VALUES(new.street_address, new.zip_code, new.x_coordinate, new.y_coordinate, new.ward, new.police_district, new.community_area, new.ssa, new.latitude, new.longitude, new.location)
            RETURNING locationID
        )
        SELECT locationID INTO _locationID FROM loc;
    END IF;

    IF _statusID IS NULL
    THEN
        WITH stat AS
        (
            INSERT INTO status(typeID, service_request_number, creation_date, status, completion_date, locationID)
            VALUES (_typeID, new.service_request_number, new.creation_date, new.status, new.completion_date, _locationID)
            RETURNING statusID
        )
        SELECT statusID INTO _statusID FROM stat;
    END IF;

    INSERT INTO graffiti_removal(statusID, what_type_of_surface_is_the_graffiti_on, where_is_the_graffiti_located)
    SELECT _statusID, new.what_type_of_surface_is_the_graffiti_on, new.where_is_the_graffiti_located;
    RETURN new;
END;
$BODY$
language plpgsql;

CREATE TRIGGER graffiti_removal_trigger
     AFTER INSERT ON graffiti_removal_tmp
     FOR EACH ROW
     EXECUTE PROCEDURE handle_insert_to_graffiti_removal_tmp();
DROP FUNCTION handle_insert_to_pot_holes_reported_tmp CASCADE;


SELECT current_time;

CREATE OR REPLACE FUNCTION handle_insert_to_pot_holes_reported_tmp() RETURNS TRIGGER AS
$BODY$
DECLARE
    _typeID int=
    (
        SELECT typeID
        FROM requests AS r
        WHERE
            r.type_of_service_request=new.type_of_service_request OR (r.type_of_service_request IS NULL AND new.type_of_service_request IS NULL)
        LIMIT 1
    );

    _locationID int=
    (
        SELECT locationID
        FROM location AS l
        WHERE
            l.location=new.location OR (l.location IS NULL AND new.location IS NULL)
            AND (l.longitude=new.longitude OR (l.longitude IS NULL AND new.longitude IS NULL))
            AND (l.latitude=new.latitude OR (l.latitude IS NULL AND new.latitude IS NULL))
            AND (l.latitude=new.latitude OR (l.latitude IS NULL AND new.latitude IS NULL))
            AND (l.x_coordinate=new.x_coordinate OR (l.x_coordinate IS NULL AND new.x_coordinate IS NULL))
            AND (l.y_coordinate=new.y_coordinate OR (l.y_coordinate IS NULL AND new.y_coordinate IS NULL))
            AND (l.street_address = new.street_address OR (l.street_address  IS NULL AND  new.street_address IS NULL))
            AND (l.zip_code=new.zip_code OR (l.zip_code IS NULL AND new.zip_code IS NULL))
            AND (l.community_area=new.community_area OR (l.community_area IS NULL AND new.community_area IS NULL))
            AND (l.ward=new.ward OR (l.ward IS NULL AND new.ward IS NULL))
            AND (l.police_district=new.police_district OR (l.police_district IS NULL AND new.police_district IS NULL))
        LIMIT 1
    );

    _statusID int=NULL;

    _actionsID int=
    (
        SELECT actionsID
        FROM actions AS a
        WHERE
            a.current_activity=new.current_activity
            AND a.most_recent_action=new.most_recent_action
        LIMIT 1
    );

BEGIN

    IF _typeID IS NOT NULL AND _locationID IS NOT NULL
    THEN
        _statusID=
        (
            SELECT statusID
            FROM status as r
            WHERE
                    (r.typeID=_typeID)
                    AND (r.locationID=_locationID)
                    AND ((r.creation_date=new.creation_date) OR (r.creation_date is null AND new.creation_date is null))
                    AND ((r.status=new.status) OR (r.status is null AND new.status is null))
                    AND ((r.completion_date=new.completion_date) OR (r.completion_date is null AND new.completion_date is null))
                    AND ((r.service_request_number=new.service_request_number) OR (r.service_request_number is null AND new.service_request_number is null))
            LIMIT 1
        );
    END IF;

    IF _typeID IS NULL
    THEN
        WITH req AS
        (
            INSERT INTO requests(type_of_service_request)
            VALUES(new.type_of_service_request)
            RETURNING typeID
        )
        SELECT typeID INTO _typeID FROM req;
    END IF;

    IF _locationID IS NULL
    THEN
        WITH loc AS
        (
            INSERT INTO location(street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, ssa, latitude, longitude, location)
            VALUES(new.street_address, new.zip_code, new.x_coordinate, new.y_coordinate, new.ward, new.police_district, new.community_area, new.ssa, new.latitude, new.longitude, new.location)
            RETURNING locationID
        )
        SELECT locationID INTO _locationID FROM loc;
    END IF;

    IF _statusID IS NULL
    THEN
        WITH stat AS
        (
            INSERT INTO status(typeID, service_request_number, creation_date, status, completion_date, locationID)
            VALUES (_typeID, new.service_request_number, new.creation_date, new.status, new.completion_date, _locationID)
            RETURNING statusID
        )
        SELECT statusID INTO _statusID FROM stat;
    END IF;

    IF _actionsID IS NULL
    THEN
        WITH act AS
        (
            INSERT INTO actions(current_activity, most_recent_action)
            VALUES (new.current_activity, new.most_recent_action)
            RETURNING actionsID
        )
        SELECT actionsID INTO _actionsID FROM act;
    END IF;

    INSERT INTO pot_holes_reported(statusID, actionsID, number_of_potholes_filled_on_block)
    SELECT _statusID, _actionsID, new.number_of_potholes_filled_on_block;
    RETURN new;
END;
$BODY$
language plpgsql;

CREATE TRIGGER pot_holes_reported_trigger
     AFTER INSERT ON pot_holes_reported_tmp
     FOR EACH ROW
     EXECUTE PROCEDURE handle_insert_to_pot_holes_reported_tmp();
DROP FUNCTION handle_insert_to_rodent_baiting_tmp CASCADE;


SELECT current_time;

CREATE OR REPLACE FUNCTION handle_insert_to_rodent_baiting_tmp() RETURNS TRIGGER AS
$BODY$
DECLARE
    _typeID int=
    (
        SELECT typeID
        FROM requests AS r
        WHERE
            r.type_of_service_request=new.type_of_service_request OR (r.type_of_service_request IS NULL AND new.type_of_service_request IS NULL)
        LIMIT 1
    );

    _locationID int=
    (
        SELECT locationID
        FROM location AS l
        WHERE
            l.location=new.location OR (l.location IS NULL AND new.location IS NULL)
            AND (l.longitude=new.longitude OR (l.longitude IS NULL AND new.longitude IS NULL))
            AND (l.latitude=new.latitude OR (l.latitude IS NULL AND new.latitude IS NULL))
            AND (l.x_coordinate=new.x_coordinate OR (l.x_coordinate IS NULL AND new.x_coordinate IS NULL))
            AND (l.y_coordinate=new.y_coordinate OR (l.y_coordinate IS NULL AND new.y_coordinate IS NULL))
            AND (l.street_address = new.street_address OR (l.street_address  IS NULL AND  new.street_address IS NULL))
            AND (l.zip_code=new.zip_code OR (l.zip_code IS NULL AND new.zip_code IS NULL))
            AND (l.community_area=new.community_area OR (l.community_area IS NULL AND new.community_area IS NULL))
            AND (l.ward=new.ward OR (l.ward IS NULL AND new.ward IS NULL))
            AND (l.police_district=new.police_district OR (l.police_district IS NULL AND new.police_district IS NULL))
        LIMIT 1
    );

    _statusID int=NULL;

    _actionsID int=
    (
        SELECT actionsID
        FROM actions AS a
        WHERE
            a.current_activity=new.current_activity
            AND a.most_recent_action=new.most_recent_action
        LIMIT 1
    );

BEGIN

    IF _typeID IS NOT NULL AND _locationID IS NOT NULL
    THEN
        _statusID=
        (
            SELECT statusID
            FROM status as r
            WHERE
                    (r.typeID=_typeID)
                    AND (r.locationID=_locationID)
                    AND ((r.creation_date=new.creation_date) OR (r.creation_date is null AND new.creation_date is null))
                    AND ((r.status=new.status) OR (r.status is null AND new.status is null))
                    AND ((r.completion_date=new.completion_date) OR (r.completion_date is null AND new.completion_date is null))
                    AND ((r.service_request_number=new.service_request_number) OR (r.service_request_number is null AND new.service_request_number is null))
            LIMIT 1
        );
    END IF;

    IF _typeID IS NULL
    THEN
        WITH req AS
        (
            INSERT INTO requests(type_of_service_request)
            VALUES(new.type_of_service_request)
            RETURNING typeID
        )
        SELECT typeID INTO _typeID FROM req;
    END IF;

    IF _locationID IS NULL
    THEN
        WITH loc AS
        (
            INSERT INTO location(street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, latitude, longitude, location)
            VALUES(new.street_address, new.zip_code, new.x_coordinate, new.y_coordinate, new.ward, new.police_district, new.community_area, new.latitude, new.longitude, new.location)
            RETURNING locationID
        )
        SELECT locationID INTO _locationID FROM loc;
    END IF;

    IF _statusID IS NULL
    THEN
        WITH stat AS
        (
            INSERT INTO status(typeID, service_request_number, creation_date, status, completion_date, locationID)
            VALUES (_typeID, new.service_request_number, new.creation_date, new.status, new.completion_date, _locationID)
            RETURNING statusID
        )
        SELECT statusID INTO _statusID FROM stat;
    END IF;

    IF _actionsID IS NULL
    THEN
        WITH act AS
        (
            INSERT INTO actions(current_activity, most_recent_action)
            VALUES (new.current_activity, new.most_recent_action)
            RETURNING actionsID
        )
        SELECT actionsID INTO _actionsID FROM act;
    END IF;

    INSERT INTO rodent_baiting(statusID, actionsID, number_of_premises_baited, number_of_premises_with_garbage, number_of_premises_with_rats)
    SELECT _statusID, _actionsID, new.number_of_premises_baited, new.number_of_premises_with_garbage, new.number_of_premises_with_rats;
    RETURN new;
END;
$BODY$
language plpgsql;

CREATE TRIGGER rodent_baiting_trigger
     AFTER INSERT ON rodent_baiting_tmp
     FOR EACH ROW
     EXECUTE PROCEDURE handle_insert_to_rodent_baiting_tmp();
DROP FUNCTION handle_insert_to_sanitation_code_complaints_tmp CASCADE;


SELECT current_time;

CREATE OR REPLACE FUNCTION handle_insert_to_sanitation_code_complaints_tmp() RETURNS TRIGGER AS
$BODY$
DECLARE
    _typeID int=
    (
        SELECT typeID
        FROM requests AS r
        WHERE
            r.type_of_service_request=new.type_of_service_request OR (r.type_of_service_request IS NULL AND new.type_of_service_request IS NULL)
        LIMIT 1
    );

    _locationID int=
    (
        SELECT locationID
        FROM location AS l
        WHERE
            l.location=new.location OR (l.location IS NULL AND new.location IS NULL)
            AND (l.longitude=new.longitude OR (l.longitude IS NULL AND new.longitude IS NULL))
            AND (l.latitude=new.latitude OR (l.latitude IS NULL AND new.latitude IS NULL))
            AND (l.x_coordinate=new.x_coordinate OR (l.x_coordinate IS NULL AND new.x_coordinate IS NULL))
            AND (l.y_coordinate=new.y_coordinate OR (l.y_coordinate IS NULL AND new.y_coordinate IS NULL))
            AND (l.street_address = new.street_address OR (l.street_address  IS NULL AND  new.street_address IS NULL))
            AND (l.zip_code=new.zip_code OR (l.zip_code IS NULL AND new.zip_code IS NULL))
            AND (l.community_area=new.community_area OR (l.community_area IS NULL AND new.community_area IS NULL))
            AND (l.ward=new.ward OR (l.ward IS NULL AND new.ward IS NULL))
            AND (l.police_district=new.police_district OR (l.police_district IS NULL AND new.police_district IS NULL))
        LIMIT 1
    );

    _statusID int=NULL;

BEGIN

    IF _typeID IS NOT NULL AND _locationID IS NOT NULL
    THEN
        _statusID=
        (
            SELECT statusID
            FROM status as r
            WHERE
                    (r.typeID=_typeID)
                    AND (r.locationID=_locationID)
                    AND ((r.creation_date=new.creation_date) OR (r.creation_date is null AND new.creation_date is null))
                    AND ((r.status=new.status) OR (r.status is null AND new.status is null))
                    AND ((r.completion_date=new.completion_date) OR (r.completion_date is null AND new.completion_date is null))
                    AND ((r.service_request_number=new.service_request_number) OR (r.service_request_number is null AND new.service_request_number is null))
            LIMIT 1
        );
    END IF;

    IF _typeID IS NULL
    THEN
        WITH req AS
        (
            INSERT INTO requests(type_of_service_request)
            VALUES(new.type_of_service_request)
            RETURNING typeID
        )
        SELECT typeID INTO _typeID FROM req;
    END IF;

    IF _locationID IS NULL
    THEN
        WITH loc AS
        (
            INSERT INTO location(street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, latitude, longitude, location)
            VALUES(new.street_address, new.zip_code, new.x_coordinate, new.y_coordinate, new.ward, new.police_district, new.community_area, new.latitude, new.longitude, new.location)
            RETURNING locationID
        )
        SELECT locationID INTO _locationID FROM loc;
    END IF;

    IF _statusID IS NULL
    THEN
        WITH stat AS
        (
            INSERT INTO status(typeID, service_request_number, creation_date, status, completion_date, locationID)
            VALUES (_typeID, new.service_request_number, new.creation_date, new.status, new.completion_date, _locationID)
            RETURNING statusID
        )
        SELECT statusID INTO _statusID FROM stat;
    END IF;

    INSERT INTO sanitation_code_complaints(statusID, what_is_the_nature_of_this_code_violation)
    SELECT _statusID, new.what_is_the_nature_of_this_code_violation;
    RETURN new;
END;
$BODY$
language plpgsql;

CREATE TRIGGER sanitation_code_complaints_trigger
     AFTER INSERT ON sanitation_code_complaints_tmp
     FOR EACH ROW
     EXECUTE PROCEDURE handle_insert_to_sanitation_code_complaints_tmp();
DROP FUNCTION handle_insert_to_street_lights_all_out_tmp CASCADE;


SELECT current_time;

CREATE OR REPLACE FUNCTION handle_insert_to_street_lights_all_out_tmp() RETURNS TRIGGER AS
$BODY$
DECLARE
    _typeID int=
    (
        SELECT typeID
        FROM requests AS r
        WHERE
            r.type_of_service_request=new.type_of_service_request OR (r.type_of_service_request IS NULL AND new.type_of_service_request IS NULL)
        LIMIT 1
    );

    _locationID int=
    (
        SELECT locationID
        FROM location AS l
        WHERE
            l.location=new.location OR (l.location IS NULL AND new.location IS NULL)
            AND (l.longitude=new.longitude OR (l.longitude IS NULL AND new.longitude IS NULL))
            AND (l.latitude=new.latitude OR (l.latitude IS NULL AND new.latitude IS NULL))
            AND (l.x_coordinate=new.x_coordinate OR (l.x_coordinate IS NULL AND new.x_coordinate IS NULL))
            AND (l.y_coordinate=new.y_coordinate OR (l.y_coordinate IS NULL AND new.y_coordinate IS NULL))
            AND (l.street_address = new.street_address OR (l.street_address  IS NULL AND  new.street_address IS NULL))
            AND (l.zip_code=new.zip_code OR (l.zip_code IS NULL AND new.zip_code IS NULL))
            AND (l.community_area=new.community_area OR (l.community_area IS NULL AND new.community_area IS NULL))
            AND (l.ward=new.ward OR (l.ward IS NULL AND new.ward IS NULL))
            AND (l.police_district=new.police_district OR (l.police_district IS NULL AND new.police_district IS NULL))
        LIMIT 1
    );

    _statusID int=NULL;

BEGIN

    IF _typeID IS NOT NULL AND _locationID IS NOT NULL
    THEN
        _statusID=
        (
            SELECT statusID
            FROM status as r
            WHERE
                    (r.typeID=_typeID)
                    AND (r.locationID=_locationID)
                    AND ((r.creation_date=new.creation_date) OR (r.creation_date is null AND new.creation_date is null))
                    AND ((r.status=new.status) OR (r.status is null AND new.status is null))
                    AND ((r.completion_date=new.completion_date) OR (r.completion_date is null AND new.completion_date is null))
                    AND ((r.service_request_number=new.service_request_number) OR (r.service_request_number is null AND new.service_request_number is null))
            LIMIT 1
        );
    END IF;

    IF _typeID IS NULL
    THEN
        WITH req AS
        (
            INSERT INTO requests(type_of_service_request)
            VALUES(new.type_of_service_request)
            RETURNING typeID
        )
        SELECT typeID INTO _typeID FROM req;
    END IF;

    IF _locationID IS NULL
    THEN
        WITH loc AS
        (
            INSERT INTO location(street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, latitude, longitude, location)
            VALUES(new.street_address, new.zip_code, new.x_coordinate, new.y_coordinate, new.ward, new.police_district, new.community_area, new.latitude, new.longitude, new.location)
            RETURNING locationID
        )
        SELECT locationID INTO _locationID FROM loc;
    END IF;

    IF _statusID IS NULL
    THEN
        WITH stat AS
        (
            INSERT INTO status(typeID, service_request_number, creation_date, status, completion_date, locationID)
            VALUES (_typeID, new.service_request_number, new.creation_date, new.status, new.completion_date, _locationID)
            RETURNING statusID
        )
        SELECT statusID INTO _statusID FROM stat;
    END IF;

    INSERT INTO street_lights_all_out(statusID)
    SELECT _statusID
    RETURN new;
END;
$BODY$
language plpgsql;

CREATE TRIGGER street_lights_all_out_trigger
     AFTER INSERT ON street_lights_all_out_tmp
     FOR EACH ROW
     EXECUTE PROCEDURE handle_insert_to_street_lights_all_out_tmp();
DROP FUNCTION handle_insert_to_street_lights_one_out_tmp CASCADE;


SELECT current_time;

CREATE OR REPLACE FUNCTION handle_insert_to_street_lights_one_out_tmp() RETURNS TRIGGER AS
$BODY$
DECLARE
    _typeID int=
    (
        SELECT typeID
        FROM requests AS r
        WHERE
            r.type_of_service_request=new.type_of_service_request OR (r.type_of_service_request IS NULL AND new.type_of_service_request IS NULL)
        LIMIT 1
    );

    _locationID int=
    (
        SELECT locationID
        FROM location AS l
        WHERE
            l.location=new.location OR (l.location IS NULL AND new.location IS NULL)
            AND (l.longitude=new.longitude OR (l.longitude IS NULL AND new.longitude IS NULL))
            AND (l.latitude=new.latitude OR (l.latitude IS NULL AND new.latitude IS NULL))
            AND (l.x_coordinate=new.x_coordinate OR (l.x_coordinate IS NULL AND new.x_coordinate IS NULL))
            AND (l.y_coordinate=new.y_coordinate OR (l.y_coordinate IS NULL AND new.y_coordinate IS NULL))
            AND (l.street_address = new.street_address OR (l.street_address  IS NULL AND  new.street_address IS NULL))
            AND (l.zip_code=new.zip_code OR (l.zip_code IS NULL AND new.zip_code IS NULL))
            AND (l.community_area=new.community_area OR (l.community_area IS NULL AND new.community_area IS NULL))
            AND (l.ward=new.ward OR (l.ward IS NULL AND new.ward IS NULL))
            AND (l.police_district=new.police_district OR (l.police_district IS NULL AND new.police_district IS NULL))
        LIMIT 1
    );

    _statusID int=NULL;

BEGIN

    IF _typeID IS NOT NULL AND _locationID IS NOT NULL
    THEN
        _statusID=
        (
            SELECT statusID
            FROM status as r
            WHERE
                    (r.typeID=_typeID)
                    AND (r.locationID=_locationID)
                    AND ((r.creation_date=new.creation_date) OR (r.creation_date is null AND new.creation_date is null))
                    AND ((r.status=new.status) OR (r.status is null AND new.status is null))
                    AND ((r.completion_date=new.completion_date) OR (r.completion_date is null AND new.completion_date is null))
                    AND ((r.service_request_number=new.service_request_number) OR (r.service_request_number is null AND new.service_request_number is null))
            LIMIT 1
        );
    END IF;

    IF _typeID IS NULL
    THEN
        WITH req AS
        (
            INSERT INTO requests(type_of_service_request)
            VALUES(new.type_of_service_request)
            RETURNING typeID
        )
        SELECT typeID INTO _typeID FROM req;
    END IF;

    IF _locationID IS NULL
    THEN
        WITH loc AS
        (
            INSERT INTO location(street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, latitude, longitude, location)
            VALUES(new.street_address, new.zip_code, new.x_coordinate, new.y_coordinate, new.ward, new.police_district, new.community_area, new.latitude, new.longitude, new.location)
            RETURNING locationID
        )
        SELECT locationID INTO _locationID FROM loc;
    END IF;

    IF _statusID IS NULL
    THEN
        WITH stat AS
        (
            INSERT INTO status(typeID, service_request_number, creation_date, status, completion_date, locationID)
            VALUES (_typeID, new.service_request_number, new.creation_date, new.status, new.completion_date, _locationID)
            RETURNING statusID
        )
        SELECT statusID INTO _statusID FROM stat;
    END IF;

    INSERT INTO street_lights_one_out(statusID)
    SELECT _statusID
    RETURN new;
END;
$BODY$
language plpgsql;

CREATE TRIGGER street_lights_one_out_trigger
     AFTER INSERT ON street_lights_one_out_tmp
     FOR EACH ROW
     EXECUTE PROCEDURE handle_insert_to_street_lights_one_out_tmp();
DROP FUNCTION handle_insert_to_tree_debris_tmp CASCADE;


SELECT current_time;

CREATE OR REPLACE FUNCTION handle_insert_to_tree_debris_tmp() RETURNS TRIGGER AS
$BODY$
DECLARE
    _typeID int=
    (
        SELECT typeID
        FROM requests AS r
        WHERE
            r.type_of_service_request=new.type_of_service_request OR (r.type_of_service_request IS NULL AND new.type_of_service_request IS NULL)
        LIMIT 1
    );

    _locationID int=
    (
        SELECT locationID
        FROM location AS l
        WHERE
            l.location=new.location OR (l.location IS NULL AND new.location IS NULL)
            AND (l.longitude=new.longitude OR (l.longitude IS NULL AND new.longitude IS NULL))
            AND (l.latitude=new.latitude OR (l.latitude IS NULL AND new.latitude IS NULL))
            AND (l.x_coordinate=new.x_coordinate OR (l.x_coordinate IS NULL AND new.x_coordinate IS NULL))
            AND (l.y_coordinate=new.y_coordinate OR (l.y_coordinate IS NULL AND new.y_coordinate IS NULL))
            AND (l.street_address = new.street_address OR (l.street_address  IS NULL AND  new.street_address IS NULL))
            AND (l.zip_code=new.zip_code OR (l.zip_code IS NULL AND new.zip_code IS NULL))
            AND (l.community_area=new.community_area OR (l.community_area IS NULL AND new.community_area IS NULL))
            AND (l.ward=new.ward OR (l.ward IS NULL AND new.ward IS NULL))
            AND (l.police_district=new.police_district OR (l.police_district IS NULL AND new.police_district IS NULL))
        LIMIT 1
    );

    _statusID int=NULL;

    _actionsID int=
    (
        SELECT actionsID
        FROM actions AS a
        WHERE
            a.current_activity=new.current_activity
            AND a.most_recent_action=new.most_recent_action
        LIMIT 1
    );

BEGIN

    IF _typeID IS NOT NULL AND _locationID IS NOT NULL
    THEN
        _statusID=
        (
            SELECT statusID
            FROM status as r
            WHERE
                    (r.typeID=_typeID)
                    AND (r.locationID=_locationID)
                    AND ((r.creation_date=new.creation_date) OR (r.creation_date is null AND new.creation_date is null))
                    AND ((r.status=new.status) OR (r.status is null AND new.status is null))
                    AND ((r.completion_date=new.completion_date) OR (r.completion_date is null AND new.completion_date is null))
                    AND ((r.service_request_number=new.service_request_number) OR (r.service_request_number is null AND new.service_request_number is null))
            LIMIT 1
        );
    END IF;

    IF _typeID IS NULL
    THEN
        WITH req AS
        (
            INSERT INTO requests(type_of_service_request)
            VALUES(new.type_of_service_request)
            RETURNING typeID
        )
        SELECT typeID INTO _typeID FROM req;
    END IF;

    IF _locationID IS NULL
    THEN
        WITH loc AS
        (
            INSERT INTO location(street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, latitude, longitude, location)
            VALUES(new.street_address, new.zip_code, new.x_coordinate, new.y_coordinate, new.ward, new.police_district, new.community_area, new.latitude, new.longitude, new.location)
            RETURNING locationID
        )
        SELECT locationID INTO _locationID FROM loc;
    END IF;

    IF _statusID IS NULL
    THEN
        WITH stat AS
        (
            INSERT INTO status(typeID, service_request_number, creation_date, status, completion_date, locationID)
            VALUES (_typeID, new.service_request_number, new.creation_date, new.status, new.completion_date, _locationID)
            RETURNING statusID
        )
        SELECT statusID INTO _statusID FROM stat;
    END IF;

    IF _actionsID IS NULL
    THEN
        WITH act AS
        (
            INSERT INTO actions(current_activity, most_recent_action)
            VALUES (new.current_activity, new.most_recent_action)
            RETURNING actionsID
        )
        SELECT actionsID INTO _actionsID FROM act;
    END IF;

    INSERT INTO tree_debris(statusID, actionsID, if_yes_where_is_the_debris_located)
    SELECT _statusID, _actionsID, new.if_yes_where_is_the_debris_located;
    RETURN new;
END;
$BODY$
language plpgsql;

CREATE TRIGGER tree_debris_trigger
     AFTER INSERT ON tree_debris_tmp
     FOR EACH ROW
     EXECUTE PROCEDURE handle_insert_to_tree_debris_tmp();

DROP FUNCTION handle_insert_to_tree_trims_tmp CASCADE;


SELECT current_time;

CREATE OR REPLACE FUNCTION handle_insert_to_tree_trims_tmp() RETURNS TRIGGER AS
$BODY$
DECLARE
    _typeID int=
    (
        SELECT typeID
        FROM requests AS r
        WHERE
            r.type_of_service_request=new.type_of_service_request OR (r.type_of_service_request IS NULL AND new.type_of_service_request IS NULL)
        LIMIT 1
    );

    _locationID int=
    (
        SELECT locationID
        FROM location AS l
        WHERE
            l.location=new.location OR (l.location IS NULL AND new.location IS NULL)
            AND (l.longitude=new.longitude OR (l.longitude IS NULL AND new.longitude IS NULL))
            AND (l.latitude=new.latitude OR (l.latitude IS NULL AND new.latitude IS NULL))
            AND (l.x_coordinate=new.x_coordinate OR (l.x_coordinate IS NULL AND new.x_coordinate IS NULL))
            AND (l.y_coordinate=new.y_coordinate OR (l.y_coordinate IS NULL AND new.y_coordinate IS NULL))
            AND (l.street_address = new.street_address OR (l.street_address  IS NULL AND  new.street_address IS NULL))
            AND (l.zip_code=new.zip_code OR (l.zip_code IS NULL AND new.zip_code IS NULL))
            AND (l.community_area=new.community_area OR (l.community_area IS NULL AND new.community_area IS NULL))
            AND (l.ward=new.ward OR (l.ward IS NULL AND new.ward IS NULL))
            AND (l.police_district=new.police_district OR (l.police_district IS NULL AND new.police_district IS NULL))
        LIMIT 1
    );

    _statusID int=NULL;

BEGIN

    IF _typeID IS NOT NULL AND _locationID IS NOT NULL
    THEN
        _statusID=
        (
            SELECT statusID
            FROM status as r
            WHERE
                    (r.typeID=_typeID)
                    AND (r.locationID=_locationID)
                    AND ((r.creation_date=new.creation_date) OR (r.creation_date is null AND new.creation_date is null))
                    AND ((r.status=new.status) OR (r.status is null AND new.status is null))
                    AND ((r.completion_date=new.completion_date) OR (r.completion_date is null AND new.completion_date is null))
                    AND ((r.service_request_number=new.service_request_number) OR (r.service_request_number is null AND new.service_request_number is null))
            LIMIT 1
        );
    END IF;

    IF _typeID IS NULL
    THEN
        WITH req AS
        (
            INSERT INTO requests(type_of_service_request)
            VALUES(new.type_of_service_request)
            RETURNING typeID
        )
        SELECT typeID INTO _typeID FROM req;
    END IF;

    IF _locationID IS NULL
    THEN
        WITH loc AS
        (
            INSERT INTO location(street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, latitude, longitude, location)
            VALUES(new.street_address, new.zip_code, new.x_coordinate, new.y_coordinate, new.ward, new.police_district, new.community_area, new.latitude, new.longitude, new.location)
            RETURNING locationID
        )
        SELECT locationID INTO _locationID FROM loc;
    END IF;

    IF _statusID IS NULL
    THEN
        WITH stat AS
        (
            INSERT INTO status(typeID, service_request_number, creation_date, status, completion_date, locationID)
            VALUES (_typeID, new.service_request_number, new.creation_date, new.status, new.completion_date, _locationID)
            RETURNING statusID
        )
        SELECT statusID INTO _statusID FROM stat;
    END IF;

    INSERT INTO tree_trims(statusID, location_of_trees)
    SELECT _statusID, new.location_of_trees;
    RETURN new;
END;
$BODY$
language plpgsql;

CREATE TRIGGER tree_trims_trigger
     AFTER INSERT ON tree_trims_tmp
     FOR EACH ROW
     EXECUTE PROCEDURE handle_insert_to_tree_trims_tmp();

SELECT current_time;
SELECT "END";
SELECT current_time;

